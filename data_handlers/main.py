from model import (User, Statistics, Referrals, UserCar, Team, UserRacer, Contract, objects, fn, DoesNotExist, Clicks)
from system.settings import *
import string
import asyncio
from system.cars import CARS
from race.race import RegularRace
import random
import datetime
from utils.helpers import get_win_by_chance, get_sum_of_skills_lvl
from race.autodromes import AUTODROMES
from utils.click import click
from response_composer import rc
from data_handlers.tasks import TasksHandler
from data_handlers.ratings import RatingsHandler
from cryptopay.cryptopay import Application


class MainHandler(TasksHandler, RatingsHandler):
    def __init__(self):
        self._update_next_race_time()
        self.is_registration_opened = True
        self.race_started = {league: True for league in LEAGUE_BUSTS.keys()}
        self.app = Application('a14686e1c630b5b2fe64217a3c483a84', callback=self.success_payment)

    async def success_payment(self, user_id):
        user = await objects.get(User, id=user_id)
        if user.vip_to > datetime.datetime.utcnow():
            user.vip_to += datetime.timedelta(days=PREM_DAYS)
        else:
            user.vip_to = datetime.datetime.utcnow() + datetime.timedelta(days=PREM_DAYS)
        await objects.update(user)
        text, _ = await rc.donation_accepted(user)
        try:
            await bot.send_message(chat_id=user.telegram_id, text=text)
        except:
            pass

    def _get_next_race_time(self, league):
        return datetime.datetime.utcnow() + datetime.timedelta(minutes=RACE_INTERVAL[league])

    def _update_next_race_time(self, league=None):
        if league:
            self.next_race_time[league] = self._get_next_race_time(league)
        else:
            self.next_race_time = {}
            for league in RACE_INTERVAL.keys():
                self.next_race_time[league] = self._get_next_race_time(league)

    async def refuel_full(self):
        await objects.execute(User.update({User.fuel: START_FUEL + User.lvl * FUEL_FOR_LVL}).where(User.is_deleted == False))

    async def race_starter(self):
        tasks = []
        for league in CAR_SM_LVL.values():
            if datetime.datetime.utcnow() >= self.next_race_time[league]:
                self._update_next_race_time(league)
                racers_ready = await objects.count(
                    UserCar.select().join(User).where(
                        (User.status == 'rest') & (UserCar.lvl == league) & User.is_participating & UserCar.is_chosen
                    )
                )
                if racers_ready >= MAX_RACERS_IN_GAME or (racers_ready >= MIN_RACERS_IN_GAME and
                                                          not self.race_started.get(league)):
                    cars = await objects.execute(UserCar.select().join(User).where(
                        (User.status == 'rest') & User.is_participating & UserCar.is_chosen & (UserCar.lvl == league)
                    ).order_by(User.vip_to.desc(), User.lvl).limit(MAX_RACERS_IN_GAME))
                    self.race_started[league] = True
                    rr = await RegularRace.create(cars=cars, league=league)
                    tasks.append(rr.start())
                else:
                    self.race_started[league] = False
        await asyncio.gather(*tasks)

    def _get_random_str(self, length=10):
        return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))

    async def _create_user(self, tg_id, username, lang):
        if not username:
            username = self._get_random_str()
        user = await objects.create(User, telegram_id=tg_id, username=username, lang=lang)
        await objects.create(Statistics, user=user)
        car = CARS[1][0]
        await objects.create(UserCar, user=user, base=car['name'], name=car['name'], max_speed=car['max_speed'], is_chosen=True,
                             acceleration=car['acceleration'], consumption=car['consumption'], lvl=1)
        return user

    async def _send_msg_new_referral(self, referred_from, referral, level):
        text, k = await rc.new_referral(referred_from, referral, level)
        try:
            await bot.send_message(chat_id=referred_from.telegram_id, text=text, reply_markup=k)
        except Exception as e:
            pass

    async def _create_referral(self, referral, referred_from, level):
        r, is_new = await objects.get_or_create(Referrals, referral=referral, referred_from=referred_from, level=level)
        if is_new:
            await self._add_canister(referred_from)
            if referred_from.ref_notify:
                await self._send_msg_new_referral(referred_from, referral, level)

    async def _process_referrals(self, u, msg):
        splited_msg = msg.text.split()
        is_with_ref = len(splited_msg) == 2
        if is_with_ref:
            link = splited_msg[1]
            referred_from = await objects.get(User, ref_link=link)
            await self._create_referral(user, referred_from, 1)
            for ref_row in await objects.execute(Referrals.select().where(Referrals.referral == referred_from)):
                    if ref_row.level < REFERRAL_LEVELS_COUNT:
                        await self._create_referral(user, ref_row.referred_from, level=ref_row.level+1)

    def _get_lang(self, msg):
        try:
            locale = msg.from_user.locale
            lang = locale.language.lower()
            if lang != 'ru' and lang != 'en':
                return 'en'
            else:
                return lang
        except:
            return 'en'

    @click
    async def start(self, user=None, msg=None):
        if user is not None:
            if user.is_deleted:
                user.is_deleted = False
                await objects.update(user)
        else:
            lang = self._get_lang(msg)
            user = await self._create_user(msg.from_user.id, msg.from_user.username, lang)
            await self._process_referrals(user, msg)

        text, k = await rc.first_msg(user=user)
        await bot.send_message(chat_id=user.telegram_id, text=text, reply_markup=k)
        return await self.profile(user=user)

    async def _add_canister(self, user: User):
        user.small_fuel_canister += 1
        await objects.update(user)
        text, k = await rc.new_canister(user=user)
        try:
            await bot.send_message(text=text, reply_markup=k, chat_id=user.telegram_id)
        except:
            pass

    async def get_online_users_count(self):
        return await objects.count(
            Clicks.select().distinct(Clicks.user_id).where(
                Clicks.date > datetime.datetime.utcnow() - datetime.timedelta(minutes=MINUTES_TO_ONLINE)
            )
        )

    @click
    async def profile(self, user):
        online = await self.get_online_users_count()
        car = await objects.get(user.cars.where(UserCar.is_chosen))
        return await rc.profile(user=user, online=online, car=car)

    @click
    async def full_profile(self, user):
        can_upgrade = user.lvl > await get_sum_of_skills_lvl(user=user)
        online = await self.get_online_users_count()
        car = await objects.get(user.cars.where(UserCar.is_chosen))
        return await rc.full_profile(user, can_upgrade, online=online, car=car)

    @click
    async def affiliate(self, user):
        stat = await objects.get(Statistics, user=user)
        refs = await objects.execute(Referrals.select(Referrals.level, fn.COUNT(Referrals.id).alias('cnt')).where(
            Referrals.referred_from == user.id).group_by(Referrals.level))
        referrals_lvl_cnt = {ref.level: ref.cnt for ref in refs}
        earned_from_ref = stat.earned_from_ref
        return await rc.affiliate(user, referrals_lvl_cnt, earned_from_ref)

    @click
    async def get_ref_link(self, user):
        b = await bot.get_me()
        return await rc.get_ref_link(user, b.username)

    async def _get_or_none_user_by_nickname(self, nick):
        try:
            result = await objects.get(User, username=nick)
        except DoesNotExist:
            result = None

        return result

    @click
    async def get_user(self, user, nickname):
        target_user = await self._get_or_none_user_by_nickname(nickname)
        if target_user:
            target_user_stat = await objects.get(Statistics, user=target_user)
        else:
            target_user_stat = None
        return await rc.get_user(user, target_user, target_user_stat)

    @click
    async def communication(self, user):
        return await rc.communication(user=user)

    @click
    async def market(self, user):
        return await rc.market(user=user)

    @click
    async def donations(self, user):
        return await rc.donations(user=user)

    @click
    async def spec_donate(self, user, symbol):
        payment = await self.app.create_payment_async(
            symbol,
            {'user_id': user.id, 'target_amount': PRICE_PREM[symbol]}
        )
        return f'<pre>{payment.address}</pre>', None

    @click
    async def pit_tires(self, user, racer_id, new_tires):
        racer: UserRacer = await objects.get(UserRacer, id=racer_id)
        racer.chosen_tires = new_tires
        racer.box_next_lap = True
        await objects.update(racer)

    @click
    async def salon(self, user):
        return await rc.salon(user=user)

    @click
    async def salon_by_lvl(self, user, lvl):
        cars = CARS.get(lvl)
        if not cars:
            return None, None
        return await rc.salon_by_lvl(user, cars, lvl)

    @click
    async def car_in_shop(self, user, car_lvl, car_index):
        car = CARS[car_lvl][car_index]
        is_car_in_garage = bool(await objects.count(user.cars.where(UserCar.base == car['name'])))
        return await rc.car_in_shop(user, car, is_car_in_garage, car_lvl, car_index)

    @click
    async def buy_car(self, user, car_lvl, car_index):
        car = CARS[car_lvl][car_index]
        is_this_car_in_garage = bool(await objects.count(user.cars.where(UserCar.base == car['name'])))
        if is_this_car_in_garage:
            return await rc.this_car_in_garage(user, car)
        elif user.money < car['price']:
            return await rc.not_enough_money_buy_car(user, car)
        else:
            user.money -= car['price']
            await objects.update(user)
            c = await objects.create(UserCar, user=user, name=car['name'], base=car['name'], max_speed=car['max_speed'],
                                     acceleration=car['acceleration'], consumption=car['consumption'], lvl=car_lvl)
            return await rc.bought_car(user, c)

    @click
    async def autos(self, user):
        cars = await objects.execute(user.cars.order_by(UserCar.is_chosen.desc(), UserCar.lvl))
        return await rc.autos(user, cars)

    @click
    async def garage(self, user):
        return await rc.garage(user=user)

    @click
    async def spares_garage(self, user):
        return await rc.spares_garage(user=user)

    @click
    async def home(self, user):
        stat = await objects.get(Statistics.select().where(Statistics.user == user))
        return await rc.home(user, stat)

    @click
    async def settings(self, user):
        return await rc.settings(user=user)

    @click
    async def change_ref_notification_status(self, user):
        user.ref_notify = not user.ref_notify
        await objects.update(user)
        return await rc.settings(user=user)

    @click
    async def change_lang(self, user, new_lang):
        user.lang = new_lang
        await objects.update(user)
        text, k = await rc.done(user=user)
        await bot.send_message(chat_id=user.telegram_id, text=text, reply_markup=k)
        return await self.home(user=user)

    @click
    async def change_nickname(self, user):
        return await rc.change_nickname(user=user)

    @click
    async def rename_car(self, user):
        return await rc.change_nickname(user=user)

    @click
    async def wrong_nickname(self, user):
        return await rc.wrong_nickname(user=user)

    @click
    async def wrong_car_name(self, user):
        return await rc.wrong_nickname(user=user)

    @click
    async def wrong_team_name(self, user):
        return await rc.create_team(user=user)

    @click
    async def wrong_team_logo(self, user, team_name):
        return await rc.process_team_name(user, team_name)

    @click
    async def wrong_offer(self, user):
        return await rc.contract_offer(user=user)

    @click
    async def process_nickname(self, user, new_nick):
        user.username = new_nick
        await objects.update(user)
        text, k = await rc.done(user=user)
        await bot.send_message(chat_id=user.telegram_id, text=text, reply_markup=k)
        return await self.settings(user=user)

    @click
    async def process_car_name(self, user, car_id, new_name):
        car = await objects.get(UserCar, id=car_id)
        car.name = new_name
        await objects.update(car)
        text, k = await rc.done(user=user)
        await bot.send_message(chat_id=user.telegram_id, text=text, reply_markup=k)
        return await self.garage(user=user)

    def is_nichname_exists(self, nick):
        is_exists = User.select().where(User.username == nick).exists()
        return is_exists

    def is_car_name_exists(self, car_name):
        is_exists = UserCar.select().where(UserCar.name == car_name).exists()
        return is_exists

    @click
    async def cancel_change_nickname(self, user):
        text, k = await rc.canceled(user=user)
        await bot.send_message(chat_id=user.telegram_id, text=text, reply_markup=k)
        return await self.settings(user=user)

    @click
    async def cancel_contract_signing(self, user):
        text, k = await rc.canceled(user=user)
        await bot.send_message(chat_id=user.telegram_id, text=text, reply_markup=k)
        return await self.team(user=user)

    @click
    async def cancel_car_renaming(self, user):
        text, k = await rc.canceled(user=user)
        await bot.send_message(chat_id=user.telegram_id, text=text, reply_markup=k)
        return await self.garage(user=user)

    @click
    async def cancel_create_team(self, user):
        text, k = await rc.canceled(user=user)
        await bot.send_message(chat_id=user.telegram_id, text=text, reply_markup=k)
        return await self.home(user=user)

    @click
    async def ratings(self, user):
        return await rc.ratings(user=user)

    @click
    async def use_canister(self, user):
        if user.small_fuel_canister > 0:
            user.small_fuel_canister -= 1
            user.fuel += CANISTER
            max_fuel = user.lvl * FUEL_FOR_LVL + START_FUEL
            if user.fuel > max_fuel:
                user.fuel = max_fuel
            await objects.update(user)
            return await self.profile(user=user)
        else:
            return await rc.error(user=user)

    @click
    async def choose_car_in_garage(self, user, car_id):
        car = await objects.get(UserCar, id=car_id)
        return await rc.car_in_garage(user, car)

    @click
    async def car_info(self, user, car_id):
        car: UserCar = await objects.get(UserCar, id=car_id)
        passed_autodromes = []
        for ad in AUTODROMES:
            if ad['speed_index'] > 5 and car.max_speed > car.acceleration:
                passed_autodromes.append(ad['name'])
            elif ad['speed_index'] < 5 and car.max_speed < car.acceleration:
                passed_autodromes.append(ad['name'])
            elif car.max_speed == car.acceleration and ad['speed_index'] in range(4, 7):
                passed_autodromes.append(ad['name'])

        return await rc.best_tracks_for_car(user, passed_autodromes)

    @click
    async def choose_car(self, user, car_id):
        car = await objects.get(UserCar, id=car_id)
        if not car.is_chosen:
            await objects.execute(UserCar.update(is_chosen=False).where(UserCar.user == user))
            await objects.execute(UserCar.update(is_chosen=True).where(UserCar.id == car_id))
            return await self.garage(user=user)
        else:
            return None, None

    @click
    async def race(self, user):
        in_queue = await objects.execute(
            UserCar.select(UserCar.lvl, fn.COUNT(User.id).alias('cnt')).join(User).where(
                (User.status == 'rest') & User.is_participating & UserCar.is_chosen
            ).group_by(UserCar.lvl).order_by(UserCar.lvl)
        )
        in_races = await objects.count(User.select().where(User.status == 'race'))
        car = await objects.get(user.cars.where(UserCar.is_chosen))
        this_hour_track = AUTODROMES[datetime.datetime.utcnow().hour % 12]
        return await rc.race(user, in_queue, in_races, self.next_race_time[car.lvl], car.lvl,
                             car.name, car.base, this_hour_track)

    @click
    async def change_race_subscribing_status(self, user):
        cars_count = await objects.count(user.cars.where(UserCar.is_chosen == True))
        if cars_count == 0 and not user.is_participating:
            return await rc.no_cars(user=user)

        car = await objects.get(user.cars.where(UserCar.is_chosen == True))
        max_fuel = car.consumption * LAPS[car.lvl]
        if user.fuel < max_fuel:
            return await rc.not_enough_fuel(user, max_fuel)
        user.is_participating = not user.is_participating
        await objects.update(user)
        return await self.race(user=user)

    @click
    async def info_track(self, user):
        this_hour_track = AUTODROMES[datetime.datetime.utcnow().hour % 12]
        return await rc.info_track(user, this_hour_track)

    @click
    async def statuses_rest(self, user):
        if user.is_admin:
            await objects.execute(User.update(status='rest'))
            return '✅', None

    @click
    async def refuel(self, user):
        if user.is_admin:
            await self.refuel_full()
            return '✅', None

    @click
    async def count(self, user):
        if user.is_admin:
            registrations = await objects.count(User.select())
            not_deleted_users = await objects.count(User.select().where(User.is_deleted == False))
            return f'{not_deleted_users}/{registrations}', None

    @click
    async def send_to_all(self, user, text):
        if user.is_admin:
            all_users = await objects.execute(User.select(User.id, User.is_deleted, User.telegram_id).where(User.is_deleted == False))
            users_count = await objects.count(User.select(User.id, User.is_deleted, User.telegram_id).where(User.is_deleted == False))
            tasks = []
            for i, user_ in enumerate(all_users, 1):
                try:
                    await bot.send_message(text=text, chat_id=user_.telegram_id)
                except Exception as e:
                    if str(e) in (
                            'Chat not found', 'Forbidden: bot was blocked by the user',
                            'Forbidden: user is deactivated'):
                        tasks.append(objects.execute(User.update(is_deleted=True).where(User.id == user_.id)))
                await asyncio.sleep(0.1)
                if i % 100 == 0:
                    await bot.send_message(text=f'✅ {i}/{users_count}', chat_id=user.telegram_id)
            await asyncio.gather(*tasks)

            return f'✅ Done!', None

    @click
    async def change_reg_status(self, user):
        if user.is_admin:
            self.is_registration_opened = not self.is_registration_opened
            text = f'current status = {self.is_registration_opened}'
            return text, None

    @click
    async def get_user_by_forward(self, user, forwarded):

        try:
            target_user = await objects.get(User, telegram_id=forwarded.id)
        except DoesNotExist:
            return 'Нет такого юзера', None

        if user.username == HELP_USERNAME:
            text = next(_ for _ in await objects.execute(User.select().where(User.telegram_id == forwarded.id).dicts()))
            k = await rc.get_k_send_canister(target_user)
            return text, k
        else:
            user_team: Team = await self._get_team(user=user)
            if user_team and user == user_team.director:
                target_user_team = await self._get_team(target_user)
                cars_cnt = await objects.count(target_user.cars)
                limit = user_team.head_office * BASE_TEAM_LVL_LIMIT
                try:
                    current_lvl_sum = await objects.get(Team.select(
                        fn.SUM(User.lvl).alias('cnt')).join(Contract).join(User).where(
                            (Contract.team == user_team) & Contract.signed & (Contract.valid_to > datetime.datetime.utcnow())
                        ).group_by(Team)
                    )
                    current_lvl_sum = current_lvl_sum.cnt
                except:
                    current_lvl_sum = 0
                return await rc.offer_by_forward(user, target_user, cars_cnt, target_user_team, limit < current_lvl_sum + target_user.lvl)
            else:
                return None, None

    async def send_canister(self, user, _id):
        if user.username == HELP_USERNAME:
            target_user = await objects.get(User, id=_id)
            await self._add_canister(target_user)
            text = f'Канистра успешно отправлена пользователю /u{target_user.username}'
            await bot.send_message(chat_id=DEV_CHAT_ID, text=text)
            k = None
            return text, k

    async def _get_team(self, user) -> Team:
        is_director = bool(await objects.count(Team.select().where(Team.director == user)))
        if is_director:
            return await objects.get(Team, director=user)
        try:
            return await objects.get(Team.select(Team).join(Contract).join(User).where(
                (Contract.valid_to > datetime.datetime.utcnow()) & (Contract.signed == True) & (Contract.racer == user)))
        except DoesNotExist:
            return None

    @click
    async def team(self, user):
        team = await self._get_team(user=user)
        if not team:
            can_create = user.money >= TEAM_REQ_MONEY and user.lvl >= TEAM_REQ_LVL
            return await rc.no_team(user, can_create)
        else:
            racers = await objects.count(
                team.contracts.where(Contract.signed & (Contract.valid_to > datetime.datetime.utcnow()))
            ) + 1

            return await rc.team(user, team, racers)

    @click
    async def create_team(self, user):
        return await rc.create_team(user=user)

    @click
    async def process_team_name(self, user, team_name):
        return await rc.process_team_name(user, team_name)

    @click
    async def process_team_logo(self, user, team_name, logo):
        if (
                not await self._get_team(user=user)
                and not await objects.count(Team.select().where((Team.name == team_name) | (Team.logo == logo)))
        ):
            user.money -= TEAM_REQ_MONEY
            await objects.update(user)
            await objects.create(Team, name=team_name, logo=logo, director=user)
            t, k = await rc.done(user=user)
            await bot.send_message(chat_id=user.telegram_id, text=t, reply_markup=k)
            return await self.team(user=user)
        else:
            return await rc.error(user, main_menu=True)

    @click
    async def upgrade_station(self, user, station):
        team: Team = await self._get_team(user=user)
        cur_lvl = getattr(team, station)
        upgr_price = TEAM_BUILDINGS_PRICE_MONEY[cur_lvl+1]
        upgr_points = TEAM_BUILDINGS_PRICE_HUMANHOURS[cur_lvl+1]
        if (
                team.director == user and
                team.building_points >= upgr_points and
                team.money >= upgr_price
        ):
            team.building_points -= upgr_points
            team.money -= upgr_price
            setattr(team, station, cur_lvl+1)
            await objects.update(team)
        return await getattr(self, station)(user=user)

    @click
    async def upgrade_skill(self, user, skill):
        total_skills_lvl = await get_sum_of_skills_lvl(user=user)
        if total_skills_lvl < user.lvl:
            cur_lvl = getattr(user, skill)
            setattr(user, skill, cur_lvl+1)
            await objects.update(user)
            text, k = await rc.done(user)
            await bot.send_message(chat_id=user.telegram_id, text=text, reply_markup=k)
        return await self.full_profile(user=user)

    @click
    async def office(self, user):
        team: Team = await self._get_team(user=user)
        return await rc.office(user, team)

    @click
    async def gas_station(self, user):
        team: Team = await self._get_team(user=user)
        return await rc.gas_station(user, team)

    @click
    async def weather_station(self, user):
        team: Team = await self._get_team(user=user)
        return await rc.weather_station(user, team)

    @click
    async def incr_staff(self, user, staff_type):
        team: Team = await self._get_team(user=user)
        if team.director == user:
            cur_cnt = getattr(team, staff_type)
            if team.money >= STAFF_SALARY:
                team.money -= STAFF_SALARY
                setattr(team, staff_type, cur_cnt + 1)
                await objects.update(team)

                return await self.office(user=user)
            else:
                return await rc.not_enough_money(user=user)
        return await rc.error(user=user)

    @click
    async def decr_staff(self, user, staff_type):
        team: Team = await self._get_team(user=user)
        if team.director == user:
            cur_cnt = getattr(team, staff_type)
            if cur_cnt > 0:
                setattr(team, staff_type, cur_cnt - 1)

                await objects.update(team)

                return await self.office(user=user)
        return await rc.error(user=user)

    @click
    async def team_info(self, user):
        team: Team = await self._get_team(user=user)
        lvl_limit = BASE_TEAM_LVL_LIMIT * team.head_office
        contracts = await objects.execute(
            team.contracts.where(Contract.signed & (Contract.valid_to > datetime.datetime.utcnow())))
        lvls_sum = sum([c.racer.lvl for c in contracts])
        racers_in_team = len([c.id for c in contracts]) + 1
        return await rc.team_info(user, team, lvl_limit, lvls_sum, racers_in_team)

    async def get_team_lvl_sum(self, team):
        try:
            current_lvl_sum = await objects.get(Team.select(
                fn.SUM(User.lvl).alias('cnt')).join(Contract).join(User).where(
                (Contract.team == team) & (Contract.signed == True) & (Contract.valid_to > datetime.datetime.utcnow())
            ).group_by(Team)
                                                )
            current_lvl_sum = current_lvl_sum.cnt
        except:
            current_lvl_sum = 0
        return current_lvl_sum

    @click
    async def contract_offer(self, user):
        return await rc.contract_offer(user=user)

    @click
    async def send_offer(self, user, uid, contract_len):
        target_user: User = await objects.get(User, id=uid)
        team = await self._get_team(user=user)
        target_user_team = await self._get_team(target_user)
        limit = team.head_office * BASE_TEAM_LVL_LIMIT
        lvl_sum = await self.get_team_lvl_sum(team)
        if (
                limit >= lvl_sum and
                team.director == user and
                MIN_CONTRACT_LEN <= contract_len <= MAX_CONTRACT_LEN and
                target_user_team is None
        ):
            valid_to = datetime.datetime.utcnow() + datetime.timedelta(days=contract_len)
            contract = await objects.create(Contract, team=team, racer=target_user, valid_to=valid_to)
            text, k = await rc.new_offer(target_user, contract)
            await bot.send_message(chat_id=target_user.telegram_id, text=text, reply_markup=k)
            return await rc.done(user=user)
        else:
            return await rc.error(user, main_menu=True)

    @click
    async def sign_contract(self, user, contract_id):
        contract = await objects.get(Contract, id=contract_id)
        target_team_lvl_sum = await self.get_team_lvl_sum(contract.team)
        limit = contract.team.head_office * BASE_TEAM_LVL_LIMIT
        team = await self._get_team(user=user)
        if (
            team is None and
            not contract.signed and
            target_team_lvl_sum + user.lvl <= limit
        ):
            contract.signed = True
            await objects.update(contract)
            text, k = await rc.contract_signed(contract.team.director, user, contract)
            await bot.send_message(chat_id=contract.team.director.telegram_id, text=text, reply_markup=k)
            return await self.team(user=user)
        else:
            return await rc.error(user=user)

    @click
    async def contract_info(self, user):
        contract = await objects.get(
            user.contracts.where(
                Contract.signed & (Contract.valid_to > datetime.datetime.utcnow())
            )
        )

        return await rc.contract_info(user, contract)

    @click
    async def contracts(self, user):
        team = await self._get_team(user=user)
        contracts = await objects.execute(
            team.contracts.where(
                Contract.signed & (Contract.valid_to > datetime.datetime.utcnow())
            )
        )

        return await rc.contracts(user, contracts)

    @click
    async def open_box(self, user, answer_func):
        if user.lootboxes > 0 and user.keys > 0:
            user.lootboxes -= 1
            user.keys -= 1

            winned = await get_win_by_chance(CHANCES)
            if winned == 'VIP':
                if user.vip_to > datetime.datetime.utcnow():
                    user.vip_to = user.vip_to + datetime.timedelta(days=VIP_FOR_DAYS)
                else:
                    user.vip_to = datetime.datetime.utcnow() + datetime.timedelta(days=VIP_FOR_DAYS)
            elif winned == 'canister':
                user.small_fuel_canister += 1
            await objects.update(user)
            await answer_func(await rc.get_opened_box_win(user, winned), show_alert=True)
            return await self.spares_garage(user=user)
        return None, None


dh = MainHandler()
