import asyncio
from model import User, Contract, Team, objects
from system.settings import FUEL_FOR_LVL, START_FUEL, REFUELING_SPEED, TEAM_DAILY_WINS, STAFF_SALARY, REFUEL_AMOUNT
from utils.logger import logger
from response_composer import rc
import datetime
from system.settings import bot



class TasksHandler:
    async def refuel_periodic(self):
        users = await objects.execute(User.select().where(
                (User.is_deleted == False) &
                (User.fuel < User.lvl * FUEL_FOR_LVL + START_FUEL)
            )
        )

        tasks = []

        for user in users:
            tasks.append(self._add_fuel(user, REFUEL_AMOUNT))
        await asyncio.gather(*tasks)
        logger.debug(f'Updating fuel periodic')

    async def refuel_gas_stations(self):
        teams = await objects.execute(Team.select())
        tasks = []
        for team in teams:
            contracts = await objects.execute(team.contracts.where(Contract.valid_to > datetime.datetime.utcnow()))
            for contract in contracts:
                tasks.append(self._add_fuel(contract.racer, REFUELING_SPEED[team.gas_station]))
            tasks.append(self._add_fuel(team.director, REFUELING_SPEED[team.gas_station]))
        await asyncio.gather(*tasks)
        logger.debug(f'Teams refueled by gas stations')

    async def update_team_points(self):
        teams = await objects.execute(Team.select().order_by(Team.points.desc()))
        tasks = []
        for place, team in enumerate(teams, 1):
            team.money += TEAM_DAILY_WINS[place]
            team.points = 0
            text = await rc.team_won_daily(team.director, place, TEAM_DAILY_WINS[place], team)
            tasks.append(bot.send_message(chat_id=team.director.telegram_id, text=text))
            await objects.update(team)
        await asyncio.gather(*tasks)
        logger.debug(f'Teams updated')


    async def teams_updater(self):
        teams = await objects.execute(Team.select())
        tasks = []
        for team in teams:
            for _ in range(team.builders):
                if team.money >= STAFF_SALARY:
                    team.money -= STAFF_SALARY
                    team.building_points += 1
            for _ in range(team.mechanics):
                team.money -= STAFF_SALARY
            tasks.append(objects.update(team))
            logger.debug(f'Team {team.name} updated')
        await asyncio.gather(*tasks)

    async def _add_fuel(self, user: User, amount):
        is_full = user.fuel == START_FUEL + user.lvl * FUEL_FOR_LVL
        if is_full:
            return
        user.fuel += amount
        if user.fuel > START_FUEL + user.lvl * FUEL_FOR_LVL:
            user.fuel = START_FUEL + user.lvl * FUEL_FOR_LVL

        is_full = user.fuel == START_FUEL + user.lvl * FUEL_FOR_LVL
        if is_full:
            text, k = await rc.fuel_full(user)
            try:
                await bot.send_message(chat_id=user.telegram_id, text=text, reply_markup=k)
            except Exception as e:
                if str(e) in (
                        'Chat not found',
                        'Forbidden: bot was blocked by the user',
                        'Forbidden: user is deactivated'
                ):
                    user.is_deleted = True
                    logger.debug(f'User {user.id} has been deleted')
                else:
                    logger.warn(f'Msg about refuel not sent to user {user.id}: {e}')
        await objects.update(user)
