from model import objects, User, Referrals, Team, Contract, fn, SQL, UserLapTime, UserRacer, Statistics, Race
from utils.click import click
from utils.helpers import get_user_place
from response_composer import rc
import datetime


class RatingsHandler:

    @click
    async def richest(self, user):
        users = await objects.execute(User.select().order_by(User.money.desc()))
        user_place, user_obj = get_user_place(user, users)
        user_logo = getattr(await self._get_team(user=user), 'logo', '')
        users_logos = [(racer, getattr(await self._get_team(user=racer), 'logo', '')) for racer in users[:5]]
        return await rc.richest(user, users_logos, user_place, user_obj, user_logo)

    @click
    async def friendly(self, user):
        users = await objects.execute(User.select(User.id, User.username, User.vip_to, fn.COUNT(Referrals.id).alias('cnt'))
                                      .join(Referrals, on=(Referrals.referred_from == User.id))
                                      .group_by(User).order_by(SQL('cnt').desc()))
        user_place, user_obj = get_user_place(user, users)
        user_logo = getattr(await self._get_team(user=user), 'logo', '')
        users_logos = [(racer, getattr(await self._get_team(user=racer), 'logo', '')) for racer in users[:5]]
        return await rc.friendly(user, users_logos, user_place, user_obj, user_logo)

    @click
    async def truckers(self, user):
        mileage = fn.SUM((Race.lap_len * Race.laps) * 0.001)
        users = await objects.execute(User.select(User.username, User.id, User.vip_to, mileage.alias('mileage')).join(UserRacer)
                                      .join(Race).group_by(User).order_by(mileage.desc()))
        user_place, user_obj = get_user_place(user, users)
        user_logo = getattr(await self._get_team(user=user), 'logo', '')
        users_logos = [(racer, getattr(await self._get_team(user=racer), 'logo', '')) for racer in users[:5]]
        return await rc.truckers(user, users_logos, user_place, user_obj, user_logo)

    @click
    async def medalists(self, user):
        medals_cnt = fn.SUM(Statistics.gold + Statistics.silver + Statistics.bronze)
        users = await objects.execute(User.select(User.id, User.username, User.vip_to,
                                                  fn.SUM(Statistics.gold).alias('gold'),
                                                  fn.SUM(Statistics.silver).alias('silver'),
                                                  fn.SUM(Statistics.bronze).alias('bronze'),
                                                  )
                                      .join(Statistics).group_by(User, Statistics)
                                      .order_by(medals_cnt.desc(), fn.SUM(Statistics.gold).desc(),
                                                fn.SUM(Statistics.silver).desc()))
        user_place, user_obj = get_user_place(user, users)
        user_logo = getattr(await self._get_team(user=user), 'logo', '')
        users_logos = [(racer, getattr(await self._get_team(user=racer), 'logo', '')) for racer in users[:5]]
        return await rc.medalists(user, users_logos, user_place, user_obj, user_logo)

    @click
    async def experienced(self, user):
        users = await objects.execute(User.select(User.id, User.vip_to, User.username, User.exp, User.lvl).order_by(User.exp.desc()))
        user_place, user_obj = get_user_place(user, users)
        user_logo = getattr(await self._get_team(user=user), 'logo', '')
        users_logos = [(racer, getattr(await self._get_team(user=racer), 'logo', '')) for racer in users[:5]]
        return await rc.experienced(user, users_logos, user_place, user_obj, user_logo)

    @click
    async def enduring(self, user):
        users = await objects.execute(User.select(User.username, User.vip_to, User.id, fn.COUNT(UserLapTime.id).alias('laps'))
                                      .join(UserRacer).join(UserLapTime)
                                      .group_by(User).order_by(SQL('laps').desc()))
        user_place, user_obj = get_user_place(user, users)
        user_logo = getattr(await self._get_team(user=user), 'logo', '')
        users_logos = [(racer, getattr(await self._get_team(user=racer), 'logo', '')) for racer in users[:5]]
        return await rc.enduring(user, users_logos, user_place, user_obj, user_logo)

    @click
    async def teams(self, user):
        teams = await objects.execute(
            Team.select(Team.name, Team.logo, Team.points, fn.COUNT(Contract.id).alias('cnt')).join(Contract).where(
                (Contract.valid_to > datetime.datetime.utcnow()) &
                (Contract.signed == True)
            ).group_by(Team.id).order_by(Team.points.desc())
        )
        return await rc.teams(user, teams)
