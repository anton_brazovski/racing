from aiogram.types.inline_keyboard import InlineKeyboardButton, InlineKeyboardMarkup
from aiogram.types.reply_keyboard import ReplyKeyboardMarkup
from translations.translations import translate
from system.settings import CAR_SM_LVL, CANISTER, ID_CHAT, INT_CHAT, RU_CHAT, STAFF_EMOJI, PRICE_PREM


class Keyboard:

    def inl_b(self, name, lang='ru', action=None, link=None, **kwargs):
        if link is not None:
            return InlineKeyboardButton(self.label(name, lang, **kwargs), url=link)

        elif action is not None:
            return InlineKeyboardButton(self.label(name, lang, **kwargs), callback_data=action)

        else:
            return InlineKeyboardButton(self.label(name, lang, **kwargs), callback_data=name)

    def label(self, name, lang, **kwargs):
        t = translate(f'menu_misc.{name}', locale=lang, **kwargs)
        if 'menu_misc' in t:
            return name
        else:
            return t

    def get_btns_for_kb(self, btns, lang):
        formed_btns = []
        for row in btns:
            formed_btns.append(['\u200c' + self.label(name, lang) for name in row])
        return formed_btns

    def get_btns_for_ik(self, btns, lang):
        formed_btns = []
        for row in btns:
            formed_btns.append([self.inl_b(name, lang) for name in row])
        return formed_btns

    async def profile(self, lang):
        ik = [[self.inl_b('full_profile', lang)]]
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def full_profile(self, lang, can_upgrade):
        if can_upgrade:
            ik = [[
                self.inl_b('💨', lang, 'upgrade_skill straights'),
                self.inl_b('🕹', lang, 'upgrade_skill turns'),
                self.inl_b('🔘', lang, 'upgrade_skill tires_control')
            ]]

            return InlineKeyboardMarkup(inline_keyboard=ik)

    async def home(self, lang):
        ik = self.get_btns_for_ik(
            [
                ['garage', 'ratings'],
                ['settings', 'team'],
                ['affiliate']
            ], lang)
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def main_menu(self, lang, points):
        btns = self.get_btns_for_kb(
            [
                ['race', 'home'],
                ['market', 'chats']
            ], lang)
        points = f'(+{points})' if points else ''
        profile = '\u200c' + self.label('profile', lang, points=points)
        btns[0].insert(0, profile)
        return ReplyKeyboardMarkup(btns, resize_keyboard=True)

    async def get_ref_link(self, lang):
        ik = [[self.inl_b('get_ref_link', lang)],
              [self.inl_b('back', lang, 'home')]]
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def invite_more(self, lang):
        ik = [[self.inl_b('invite_more', lang, 'get_ref_link')]]
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def communication_links(self, lang):
        ik = [
            [self.inl_b('updates_channel', lang, link='t.me/racing_upd')],
            [self.inl_b('int_chat', lang, link=INT_CHAT)],
            [self.inl_b('ru_flag', lang='ru', link=RU_CHAT),
             self.inl_b('id_flag', lang='ru', link=ID_CHAT)]
        ]
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def cars_shop(self, lang, cars):
        ik = [[self.inl_b('car_in_shop', lang, action=f'car_in_shop {car.id}', car_name=car.name)] for car in cars]
        ik.append([self.inl_b('back', lang, 'market')])
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def car_in_shop(self, lang, is_in_garage, car_lvl, car_index, can_buy):
        if is_in_garage:
            ik = [[self.inl_b('car_already_in_garage', lang)]]
        elif can_buy:
            ik = [[self.inl_b(f'buy_car', lang, f'buy_car {car_lvl} {car_index}')]]
        else:
            ik = []
        ik.append([self.inl_b('back', lang, f'shop_cars {car_lvl}')])
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def spares_garage(self, lang, user):
        ik = []
        if user.small_fuel_canister > 0:
            ik.append([self.inl_b('use_canister', lang, f'use_canister small', bonus=CANISTER)])
        if user.keys > 0 and user.lootboxes > 0:
            ik.append([self.inl_b('open_box', lang)])
        ik.append([self.inl_b('get_ref_link', lang)])
        ik.append([self.inl_b('back', lang, 'garage')])
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def garage(self, lang):
        ik = self.get_btns_for_ik([['autos']], lang)
        ik[0].append(self.inl_b('spares', lang, 'spares_garage'))
        ik.append([self.inl_b('back', lang, 'home')])
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def autos(self, lang, cars):
        ik = []
        for car in cars:
            ik.append([self.inl_b('car_garage', lang, f'choose_car_in_garage {car.id}',
                                  car_name=car.name)])
        ik.append([self.inl_b('back', lang, 'garage')])
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def car_in_garage(self, lang, car, is_vip):
        ik = [
            [self.inl_b('info', lang, f'info {car.id}')],
            [self.inl_b('back', lang, 'autos'), self.inl_b('spares', lang, f'spares {car.id}')]
        ]

        if car.name == car.base and is_vip:
            ik[0].insert(0, self.inl_b('rename', lang, f'rename_car {car.id}'))

        if not car.is_chosen:
            ik.insert(0, [self.inl_b('choose_car', lang, f'choose_car {car.id}')])

        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def race(self, lang, is_part):
        if is_part:
            ik = [[self.inl_b('unsubscribe_race', lang, 'change_race_subscribing_status')]]
        else:
            ik = [[self.inl_b('subscribe_race', lang, 'change_race_subscribing_status')]]

        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def settings(self, lang, is_vip):
        ik = [[self.inl_b('ru_flag', lang='ru', action='lang ru'), self.inl_b('en_flag', lang='ru', action='lang en')],
              [self.inl_b('change_ref_notify', lang)],
              [self.inl_b('back', lang, 'home')]]
        if is_vip:
            ik.insert(-2, [self.inl_b('change_nickname', lang)])
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def ratings(self, lang):
        ik = self.get_btns_for_ik(
            [
                ['richest', 'experienced'],
                ['truckers', 'enduring'],
                ['teams', 'medalists'],
                ['friendly']
            ], lang)
        ik[-1].insert(0, self.inl_b('back', lang, 'home'))
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def back_to_ratings(self, lang):
        ik = [[self.inl_b('back', lang, 'ratings')]]
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def bought_car(self, lang):
        ik = self.get_btns_for_ik([['race', 'home']], lang)
        ik.append([self.inl_b('back', lang, 'salon')])
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def cancel(self, lang):
        btns = self.get_btns_for_kb([['cancel']], lang)
        return ReplyKeyboardMarkup(btns, resize_keyboard=True)

    async def inline_market(self, lang):
        ik = self.get_btns_for_ik([['market']], lang)
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def market(self, lang):
        ik = self.get_btns_for_ik([['salon', 'shop'], ['donations']], lang)
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def salon(self, lang):
        ik = InlineKeyboardMarkup(row_width=5)
        base_btns = [self.inl_b(sm, lang, f'shop_cars {lvl}') for sm, lvl in CAR_SM_LVL.items()]
        base_btns.append(self.inl_b('back', lang, 'market'))
        ik.add(*base_btns)
        return ik

    async def get_cars(self, lang, cars, lvl):
        ik = InlineKeyboardMarkup()
        for index, car in enumerate(cars):
            b = self.inl_b(car['name'], lang, f'choose_shop_car {lvl} {index}')
            ik.add(b)
        ik.add(self.inl_b('back', lang, 'salon'))
        return ik

    async def donations(self, lang):
        ik = [
            [self.inl_b(f'💎 {price} {symbol.upper()}', lang, f'donation {symbol}')]
            for symbol, price in PRICE_PREM.items()
        ]
        ik.append([self.inl_b('back', lang, 'market')])

        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def medal_home(self, lang):
        ik = [[self.inl_b('home', lang)]]
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def use_canister(self, lang):
        ik = [[self.inl_b('use_canister', lang, f'use_canister', bonus=CANISTER)]]
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def send_canister(self, to_u_id):
        ik = [[self.inl_b('send_canister', 'ru', f'send_canister {to_u_id}')]]
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def no_team(self, lang, can_create_team):
        ik = []
        if can_create_team:
            ik.append([self.inl_b('create_team', lang)])
        ik.append([self.inl_b('int_chat', lang, link=INT_CHAT), self.inl_b('ru_flag', lang='ru', link=RU_CHAT),
                   self.inl_b('id_flag', lang='ru', link=ID_CHAT)])
        ik.append([self.inl_b('back', lang, 'home')])
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def team(self, lang, team):
        ik = self.get_btns_for_ik([['office', 'gas_station', 'weather_station'],
                                   ['team_info']], lang)
        if team.chat_link:
            ik[1].append(self.inl_b('team_chat', lang, link=team.chat_link))
        ik[1].insert(0, self.inl_b('back', lang, 'home'))
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def _get_row_staff(self, stype, team):
        decr_b = self.inl_b('decr', 'ru', f'decr {stype}')
        incr_b = self.inl_b('incr', 'ru', f'incr {stype}')
        cnt_btn = self.inl_b(f'{STAFF_EMOJI[stype[:-1]]} ({getattr(team, stype)})', action='staff_emo')
        return [decr_b, cnt_btn, incr_b]

    async def office(self, lang, user, team, upgrade_cost, upgrade_build_points):
        ik = [[self.inl_b('back', lang, 'team')]]
        if user == team.director:
            ik.insert(0, [self.inl_b('contracts', lang, 'contracts')])
            if team.money >= upgrade_cost and team.building_points >= upgrade_build_points:
                b = 'upgrade' if team.gas_station > 0 else 'build'
                ik[0].insert(0, self.inl_b(b, lang, 'upgrade head_office'))
            ik.insert(-2, await self._get_row_staff('managers', team))
            ik.insert(-2, await self._get_row_staff('mechanics', team))
            ik.insert(-2, await self._get_row_staff('builders', team))

        else:
            ik.insert(0, [self.inl_b('contract_info', lang, 'contract_info')])
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def gas_station(self, lang, user, team, upgrade_cost, upgrade_build_points):
        ik = [[self.inl_b('back', lang, 'team')]]
        if user == team.director:
            if team.money >= upgrade_cost and team.building_points >= upgrade_build_points:
                b = 'upgrade' if team.gas_station > 0 else 'build'
                ik.insert(0, [self.inl_b(b, lang, 'upgrade gas_station')])
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def weather_station(self, lang, user, team, upgrade_cost, upgrade_build_points):
        ik = [[self.inl_b('back', lang, 'team')]]
        if user == team.director:
            if team.money >= upgrade_cost and team.building_points >= upgrade_build_points:
                b = 'upgrade' if team.gas_station > 0 else 'build'
                ik[0].insert(0, self.inl_b(b, lang, 'upgrade weather_station'))
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def offer_by_forward(self, lang, u):
        ik = [[self.inl_b('offer_contract', lang, f'offer_contract {u.id}')]]
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def sign_contract(self, lang, contract_id):
        ik = [[self.inl_b('sign_contract', lang, f'sign_contract {contract_id}')]]
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def fuel_full(self, lang):
        ik = [[self.inl_b('race', lang)]]
        return InlineKeyboardMarkup(inline_keyboard=ik)

    async def tires_race(self, tires, racer_id):
        ik = [[self.inl_b(tire, 'en', f'tires {racer_id} {tire}') for tire in tires]]
        return InlineKeyboardMarkup(inline_keyboard=ik)

