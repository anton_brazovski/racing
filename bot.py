import emoji
from system.settings import *
from aiogram import types
from aiogram.utils import executor
from data_handlers.main import dh
from translations.translations import sm
import re
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from utils.helpers import rate_limit, ThrottlingMiddleware, get_next_hour
from cryptopay.cryptopay import Application
import asyncio

USERNAME = 'process_username'
CAR_NAME = 'process_car_name'
TEAM_NAME = 'process_team_name'
TEAM_LOGO = 'process_team_logo'
CONTRACT = 'process_contract_len'


@dp.message_handler(commands=['start'])
@rate_limit(.5)
async def start(message: types.Message):
    text, k = await dh.start(msg=message)
    await message.reply(text, reply_markup=k, reply=False)


@dp.message_handler(lambda msg: msg.text.startswith(sm('profile')))
@rate_limit(3)
async def profile(message: types.Message):
    text, k = await dh.profile(msg=message)
    await message.reply(text, reply_markup=k, reply=False)


@dp.callback_query_handler(lambda msg: msg.data == 'full_profile')
@rate_limit(3)
async def full_profile(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.full_profile(msg=message)
    await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data == 'affiliate')
@rate_limit(3)
async def affiliate(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.affiliate(msg=message)
    await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data == 'get_ref_link')
@rate_limit(3)
async def full_profile(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.get_ref_link(msg=message)
    await message.message.reply(text, reply_markup=k, reply=None)


@dp.message_handler(lambda msg: msg.text.startswith('/u'))
@rate_limit(1)
async def get_user(message: types.Message):
    text, k = await dh.get_user(msg=message, nickname=message.text[2:])
    await message.reply(text, reply_markup=k, reply=False)


@dp.message_handler(lambda msg: msg.text.startswith(sm('chats')))
@rate_limit(3)
async def communication(message: types.Message):
    text, k = await dh.communication(msg=message)
    await message.reply(text, reply_markup=k, reply=False)


@dp.message_handler(lambda msg: msg.text.startswith(sm('market')))
@rate_limit(3)
async def market(message: types.Message):
    text, k = await dh.market(msg=message)
    await message.reply(text, reply_markup=k, reply=False)


@dp.callback_query_handler(lambda msg: msg.data == 'market')
@rate_limit(3)
async def market_inline(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.market(msg=message)
    await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data == 'salon')
@rate_limit(3)
async def salon(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.salon(msg=message)
    await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data == 'shop')
@rate_limit(3)
async def shop(message: types.CallbackQuery):
    await message.answer('Will be added in the next update', show_alert=True)
    # text, k = await dh.shop(msg=message)
    # await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: re.match('^shop_cars [0-9]+$', msg.data))
@rate_limit(3)
async def salon_by_lvl(message: types.CallbackQuery):
    lvl = int(message.data.split()[-1])
    text, k = await dh.salon_by_lvl(msg=message, lvl=lvl)
    if not text:
        await message.answer('Will be added!', show_alert=True)
    elif message.message.text != text:
        await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: re.match('^choose_shop_car [0-9]+ [0-9]$', msg.data))
@rate_limit(3)
async def car_in_shop(message: types.CallbackQuery):
    await message.answer()
    car_lvl, car_index = map(int, message.data.split()[1:])
    text, k = await dh.car_in_shop(msg=message, car_lvl=car_lvl, car_index=car_index)
    await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: re.match('^buy_car [0-9]+ [0-9]$', msg.data))
@rate_limit(3)
async def buy_car(message: types.CallbackQuery):
    await message.answer()
    car_lvl, car_index = map(int, message.data.split()[1:])
    text, k = await dh.buy_car(msg=message, car_lvl=car_lvl, car_index=car_index)
    if text:
        await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data == 'autos')
@rate_limit(3)
async def autos(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.autos(msg=message)
    await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data == 'garage')
@rate_limit(3)
async def garage(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.garage(msg=message)
    await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data == 'spares_garage')
@rate_limit(3)
async def spares_garage(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.spares_garage(msg=message)
    await message.message.edit_text(text, reply_markup=k)


@dp.message_handler(lambda msg: msg.text.startswith(sm('home')))
@rate_limit(3)
async def home(message: types.Message):
    text, k = await dh.home(msg=message)
    await message.reply(text, reply_markup=k, reply=False)


@dp.callback_query_handler(lambda msg: msg.data == 'home')
@rate_limit(3)
async def home_inline(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.home(msg=message)
    await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data == 'team')
@rate_limit(3)
async def team(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.team(msg=message)
    await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data == 'create_team')
@rate_limit(3)
async def create_team(message: types.CallbackQuery):
    await message.answer()
    state = dp.current_state(chat=message.message.chat.id, user=message.from_user.id)
    await state.set_state(TEAM_NAME)
    text, k = await dh.create_team(msg=message)
    await message.message.reply(text, reply_markup=k, reply=False)


@dp.message_handler(state=TEAM_NAME, func=lambda msg: msg.text.startswith(sm('cancel')))
@dp.message_handler(state=TEAM_LOGO, func=lambda msg: msg.text.startswith(sm('cancel')))
async def cancel_create_team(message: types.Message):
    with dp.current_state(chat=message.chat.id, user=message.from_user.id) as state:
        if await state.get_state() is None:
            return

        await state.reset_state(with_data=True)
    text, k = await dh.cancel_create_team(msg=message)
    await message.reply(text, reply_markup=k, reply=False)


@dp.message_handler(state=TEAM_NAME, func=lambda msg: re.match('^[a-zA-Z0-9 ]{5,15}$', msg.text))
async def process_team_name(message: types.Message):
    with dp.current_state(chat=message.chat.id, user=message.from_user.id) as state:
        await state.set_state(TEAM_LOGO)
        await state.update_data({'team_name': message.text})

    text, k = await dh.process_team_name(msg=message, team_name=message.text)
    await message.reply(text, reply_markup=k, reply=False)


@dp.message_handler(state=TEAM_LOGO, func=lambda msg: msg.text in emoji.UNICODE_EMOJI)
async def process_team_logo(message: types.Message):
    state = dp.current_state(chat=message.chat.id, user=message.from_user.id)
    data = await state.get_data()
    logo = message.text
    text, k = await dh.process_team_logo(msg=message, team_name=data['team_name'], logo=logo)
    await message.reply(text, reply_markup=k, reply=False)
    await state.finish()


@dp.message_handler(state=TEAM_NAME, func=lambda msg: not re.match('^[a-zA-Z0-9 ]{5,15}$', msg.text))
async def wrong_team_name(message: types.Message):
    text, k = await dh.wrong_team_name(msg=message)
    await message.reply(text, reply_markup=k, reply=False)


@dp.message_handler(state=TEAM_LOGO, func=lambda msg: msg.text not in emoji.UNICODE_EMOJI)
async def wrong_team_logo(message: types.Message):
    state = dp.current_state(chat=message.chat.id, user=message.from_user.id)
    data = await state.get_data()
    text, k = await dh.wrong_team_logo(msg=message, team_name=data['team_name'])
    await message.reply(text, reply_markup=k, reply=False)


@dp.callback_query_handler(lambda msg: msg.data.startswith('upgrade '))
@rate_limit(3)
async def upgrade_station(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.upgrade_station(msg=message, station=message.data.split()[1])
    if text != message.message.text:
        await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data.startswith('upgrade_skill '))
@rate_limit(3)
async def upgrade_skill(message: types.CallbackQuery):
    await message.answer()
    skill = message.data.split()[1]
    text, k = await dh.upgrade_skill(msg=message, skill=skill)
    if text != message.message.text:
        await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data == 'office')
@rate_limit(3)
async def office(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.office(msg=message)
    if text != message.message.text:
        await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data == 'gas_station')
@rate_limit(3)
async def gas_station(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.gas_station(msg=message)
    if text != message.message.text:
        await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data == 'weather_station')
@rate_limit(3)
async def weather_station(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.weather_station(msg=message)
    if text != message.message.text:
        await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data == 'staff_emo')
@rate_limit(1)
async def staff_emo(message: types.CallbackQuery):
    await message.answer()


@dp.callback_query_handler(lambda msg: msg.data.startswith('incr '))
@rate_limit(1)
async def incr_staff(message: types.CallbackQuery):
    staff_type = message.data.split()[-1]
    text, k = await dh.incr_staff(msg=message, staff_type=staff_type)
    if k:
        await message.answer()
        await message.message.edit_text(text, reply_markup=k)
    else:
        await message.answer(text, show_alert=True)


@dp.callback_query_handler(lambda msg: msg.data.startswith('decr '))
@rate_limit(1)
async def decr_staff(message: types.CallbackQuery):
    staff_type = message.data.split()[-1]
    text, k = await dh.decr_staff(msg=message, staff_type=staff_type)
    if k:
        await message.answer()
        await message.message.edit_text(text, reply_markup=k)
    else:
        await message.answer(text, show_alert=True)


@dp.callback_query_handler(lambda msg: msg.data == 'team_info')
@rate_limit(1)
async def team_info(message: types.CallbackQuery):
    text, k = await dh.team_info(msg=message)
    if k:
        await message.answer()
        await message.message.edit_text(text, reply_markup=k)
    else:
        await message.answer(text, show_alert=True)


@dp.callback_query_handler(lambda msg: msg.data == 'contract_info')
@rate_limit(1)
async def contract_info(message: types.CallbackQuery):
    text, k = await dh.contract_info(msg=message)
    await message.answer(text, show_alert=True)



@dp.callback_query_handler(lambda msg: msg.data == 'contracts')
@rate_limit(1)
async def contracts(message: types.CallbackQuery):
    text, k = await dh.contracts(msg=message)
    await message.answer(text, show_alert=True)


@dp.callback_query_handler(lambda msg: msg.data.startswith('offer_contract '))
@rate_limit(1)
async def contract_offer(message: types.CallbackQuery):
    await message.answer()
    with dp.current_state(chat=message.message.chat.id, user=message.from_user.id) as state:
        await state.set_state(CONTRACT)
        u_id = int(message.data.split()[-1])
        await state.update_data({'u_id': u_id})
    text, k = await dh.contract_offer(msg=message)
    await message.message.reply(text, reply_markup=k, reply=False)


@dp.message_handler(state=CONTRACT, func=lambda msg: re.match('^[0-9]{1,2}$', msg.text))
@rate_limit(1)
async def send_offer(message: types.Message):
    state = dp.current_state(chat=message.chat.id, user=message.from_user.id)
    data = await state.get_data()
    text, k = await dh.send_offer(msg=message, uid=data['u_id'], contract_len=int(message.text))
    await message.reply(text, reply_markup=k, reply=False)
    await state.finish()


@dp.message_handler(state=CONTRACT, func=lambda msg: msg.text.startswith(sm('cancel')))
async def cancel_contract_signing(message: types.Message):
    with dp.current_state(chat=message.chat.id, user=message.from_user.id) as state:
        if await state.get_state() is None:
            return

        await state.reset_state(with_data=True)
    text, k = await dh.cancel_contract_signing(msg=message)
    await message.reply(text, reply_markup=k, reply=False)


@dp.message_handler(state=CONTRACT, func=lambda msg: not re.match('^[0-9]{1,2}$', msg.text))
@rate_limit(1)
async def wrong_offer(message: types.Message):
    text, k = await dh.wrong_offer(msg=message)
    await message.reply(text, reply_markup=k, reply=False)


@dp.callback_query_handler(lambda msg: msg.data.startswith('sign_contract '))
@rate_limit(1)
async def sign_contract(message: types.CallbackQuery):
    await message.answer()
    contract_id = int(message.data.split()[1])
    text, k = await dh.sign_contract(msg=message, contract_id=contract_id)
    await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data == 'ratings')
@rate_limit(3)
async def ratings(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.ratings(msg=message)
    await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data == 'settings')
@rate_limit(3)
async def settings(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.settings(msg=message)
    await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data == 'change_ref_notify')
@rate_limit(3)
async def change_ref_notification_status(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.change_ref_notification_status(msg=message)
    await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: re.match('^lang [a-z]{2}$', msg.data))
@rate_limit(3)
async def change_lang(message: types.CallbackQuery):
    await message.answer()
    new_lang = message.data.split()[-1]
    text, k = await dh.change_lang(msg=message, new_lang=new_lang)
    await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data == 'change_nickname')
@rate_limit(3)
async def change_nickname(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.change_nickname(msg=message)
    state = dp.current_state(chat=message.message.chat.id, user=message.from_user.id)
    await state.set_state(USERNAME)
    await message.message.reply(text, reply_markup=k, reply=False)


@dp.message_handler(state=USERNAME, func=lambda msg: msg.text.startswith(sm('cancel')))
async def cancel_change_nickname(message: types.Message):
    with dp.current_state(chat=message.chat.id, user=message.from_user.id) as state:
        if await state.get_state() is None:
            return

        await state.reset_state(with_data=True)
        text, k = await dh.cancel_change_nickname(msg=message)
        await message.reply(text, reply_markup=k, reply=False)


@dp.message_handler(
    state=USERNAME,
    func=lambda msg:
    re.match('^[a-zA-Z0-9_]{2,15}$', msg.text) and not dh.is_nichname_exists(msg.text))
async def process_username(message: types.Message):
    state = dp.current_state(chat=message.chat.id, user=message.from_user.id)
    await state.finish()
    text, k = await dh.process_nickname(msg=message, new_nick=message.text)
    await message.reply(text, reply_markup=k, reply=False)


@dp.message_handler(state=USERNAME,
                    func=lambda msg:
                    (not re.match('^[a-zA-Z0-9_]{2,15}$', msg.text)) or dh.is_nichname_exists(msg.text))
async def wrong_username(message: types.Message):
    text, k = await dh.wrong_nickname(msg=message)
    await message.reply(text, reply_markup=k, reply=False)


@dp.callback_query_handler(lambda msg: msg.data.startswith('rename_car'))
@rate_limit(3)
async def rename_car(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.rename_car(msg=message)
    with dp.current_state(chat=message.message.chat.id, user=message.from_user.id) as state:
        await state.set_state(CAR_NAME)
        car_id = int(message.data.split()[-1])
        await state.update_data({'car_id': car_id})
    await message.message.reply(text, reply_markup=k, reply=False)


@dp.message_handler(state=CAR_NAME, func=lambda msg: msg.text.startswith(sm('cancel')))
async def cancel_car_renaming(message: types.Message):
    with dp.current_state(chat=message.chat.id, user=message.from_user.id) as state:
        if await state.get_state() is None:
            return

        await state.reset_state(with_data=True)
        text, k = await dh.cancel_car_renaming(msg=message)
        await message.reply(text, reply_markup=k, reply=False)


@dp.message_handler(state=CAR_NAME,
                    func=lambda msg:
                    re.match('^[a-zA-Z0-9_]{2,15}$', msg.text) and not dh.is_car_name_exists(msg.text))
async def process_car_name(message: types.Message):
    state = dp.current_state(chat=message.chat.id, user=message.from_user.id)
    data = await state.get_data()
    text, k = await dh.process_car_name(message, car_id=data['car_id'], new_name=message.text)
    await state.finish()
    await message.reply(text, reply_markup=k, reply=False)


@dp.message_handler(state=CAR_NAME,
                    func=lambda msg:
                    (not re.match('^[a-zA-Z0-9_]{2,15}$', msg.text)) or dh.is_car_name_exists(msg.text))
async def wrong_username(message: types.Message):
    text, k = await dh.wrong_car_name(msg=message)
    await message.reply(text, reply_markup=k, reply=False)


@dp.callback_query_handler(lambda msg: re.match('^choose_car_in_garage [0-9]+$', msg.data))
@rate_limit(3)
async def choose_car_in_garage(message: types.CallbackQuery):
    await message.answer()
    car_id = int(message.data.split()[1])
    text, k = await dh.choose_car_in_garage(msg=message, car_id=car_id)
    if text:
        await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: re.match('^spares [0-9]+$', msg.data))
@rate_limit(3)
async def spares(message: types.CallbackQuery):
    await message.answer('Will be added', show_alert=True)
    # text, k = await dh.spares(msg=message)
    # if text:
    #     await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: re.match('^info [0-9]+$', msg.data))
@rate_limit(3)
async def car_info(message: types.CallbackQuery):
    text, _ = await dh.car_info(msg=message, car_id = int(message.data.split()[-1]))
    await message.answer(text, show_alert=True)


@dp.callback_query_handler(lambda msg: re.match('^choose_car [0-9]+$', msg.data))
@rate_limit(3)
async def choose_car(message: types.CallbackQuery):
    await message.answer()
    car_id = int(message.data.split()[1])
    text, k = await dh.choose_car(msg=message, car_id=car_id)
    await message.message.edit_text(text, reply_markup=k)


@dp.message_handler(lambda msg: msg.text.startswith(sm('race')))
@rate_limit(3)
async def race(message: types.Message):
    text, k = await dh.race(msg=message)
    await message.reply(text, reply_markup=k, reply=False)


@dp.callback_query_handler(lambda msg: msg.data == 'race')
@rate_limit(3)
async def race_inline(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.race(msg=message)
    await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: re.match('^change_race_subscribing_status$', msg.data))
@rate_limit(3)
async def change_race_subscribing_status(message: types.CallbackQuery):
    if dh.is_registration_opened:
        await message.answer()
        text, k = await dh.change_race_subscribing_status(msg=message)
        if text != message.message.text:
            await message.message.edit_text(text, reply_markup=k)
    else:
        await message.answer('Registration closed. It might be an update 🤔')


@dp.callback_query_handler(lambda msg: msg.data == 'richest')
@rate_limit(3)
async def richest(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.richest(msg=message)
    await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data == 'friendly')
@rate_limit(3)
async def friendly(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.friendly(msg=message)
    await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data == 'truckers')
@rate_limit(3)
async def truckers(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.truckers(msg=message)
    await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data == 'enduring')
@rate_limit(3)
async def enduring(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.enduring(msg=message)
    await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data == 'teams')
@rate_limit(3)
async def teams(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.teams(msg=message)
    if text:
        await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data == 'medalists')
@rate_limit(3)
async def medalists(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.medalists(msg=message)
    await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data == 'experienced')
@rate_limit(3)
async def experienced(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.experienced(msg=message)
    await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data.startswith('use_canister'))
@rate_limit(.5)
async def use_canister(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.use_canister(msg=message)
    await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data == 'open_box')
@rate_limit(.5)
async def open_box(message: types.CallbackQuery):
    text, k = await dh.open_box(msg=message, answer_func=message.answer)
    await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data == 'donations')
@rate_limit(3)
async def donations(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.donations(msg=message)
    await message.message.edit_text(text, reply_markup=k)


@dp.callback_query_handler(lambda msg: msg.data.startswith('donation '))
@rate_limit(2)
async def spec_donate(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.spec_donate(msg=message, symbol=message.data.split()[1])
    await message.message.reply(text, reply_markup=k, reply=False)


@dp.callback_query_handler(lambda msg: msg.data.startswith('tires '))
@rate_limit(2)
async def pit_tires(message: types.CallbackQuery):
    await message.answer()
    racer_id, new_tires = message.data.split()[1:]
    await dh.pit_tires(msg=message, racer_id=int(racer_id), new_tires=new_tires)
    await bot.edit_message_reply_markup(chat_id=message.from_user.id, message_id=message.message.message_id,
                                        reply_markup=None)


@dp.message_handler(commands=['info_track'])
@rate_limit(.5)
async def info_track(message: types.Message):
    text, k = await dh.info_track(msg=message)
    await message.reply(text, reply_markup=k, reply=False)


@dp.message_handler(commands=['statuses_rest'])
@rate_limit(.5)
async def statuses_rest(message: types.Message):
    text, k = await dh.statuses_rest(msg=message)
    await message.reply(text, reply_markup=k, reply=False)


@dp.message_handler(commands=['refuel'])
@rate_limit(.5)
async def refuel(message: types.Message):
    text, k = await dh.refuel(msg=message)
    await message.reply(text, reply_markup=k, reply=False)


@dp.message_handler(commands=['count'])
@rate_limit(.5)
async def count(message: types.Message):
    text, k = await dh.count(msg=message)
    await message.reply(text, reply_markup=k, reply=False)


@dp.message_handler(commands=['send_to_all'])
@rate_limit(.5)
async def send_to_all(message: types.Message):
    text, k = await dh.send_to_all(msg=message, text=message.text[12:])
    await message.reply(text, reply_markup=k, reply=False)


@dp.message_handler(commands=['change_reg_status'])
@rate_limit(.5)
async def change_reg_status(message: types.Message):
    text, k = await dh.change_reg_status(msg=message)
    await message.reply(text, reply_markup=k, reply=False)


@dp.message_handler(lambda msg: msg.forward_from)
async def get_user_by_forward(message: types.Message):
    text, k = await dh.get_user_by_forward(msg=message, forwarded=message.forward_from)
    if text:
        await message.reply(text, reply_markup=k, reply=False)


@dp.callback_query_handler(lambda msg: msg.data.startswith('send_canister '))
@rate_limit(5)
async def send_canister(message: types.CallbackQuery):
    await message.answer()
    text, k = await dh.send_canister(msg=message, _id=int(message.data.split()[-1]))
    if text:
        await message.message.edit_text(text, reply_markup=k)


async def shutdown(dispatcher: Dispatcher):
    await dispatcher.storage.close()
    await dispatcher.storage.wait_closed()


if __name__ == '__main__':
    dp.middleware.setup(ThrottlingMiddleware())
    scheduler = AsyncIOScheduler(event_loop=loop)
    scheduler.add_job(dh.refuel_periodic, 'interval', minutes=15, next_run_time=get_next_hour())
    scheduler.add_job(dh.refuel_gas_stations, 'interval', hours=1, next_run_time=get_next_hour())
    scheduler.add_job(dh.update_team_points, 'cron', hour='00', minute='00')
    scheduler.add_job(dh.race_starter, 'interval', minutes=1, max_instances=15)
    scheduler.add_job(dh.teams_updater, 'interval', hours=1, max_instances=1, next_run_time=get_next_hour())
    scheduler.start()
    executor.start_polling(dp, loop=loop, on_shutdown=shutdown)
