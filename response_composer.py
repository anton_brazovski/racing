from translations.translations import translate
from keyboards import Keyboard
from system.settings import (
    REFERRAL_INCOME, ACTIVE_OR_NOT_SM, LANG_FLAGS, USERS_IN_TOP_RATING, COUNTRY_FLAGS, WINNERS_MEDALS, MONEY_FOR_RACE,
    EXP_FOR_PLACE, CAR_LVL_SM, LEAGUE_BUSTS, TEAM_REQ_LVL, TEAM_REQ_MONEY, TEAM_BUILDINGS_PRICE_HUMANHOURS,
    TEAM_BUILDINGS_PRICE_MONEY, REFUELING_SPEED, METEO_POWER, LOOTBOX, KEY, MIN_CONTRACT_LEN, MAX_CONTRACT_LEN,
    PLAIN_ACCOUNT_PREFIX, VIP_ACCOUNT_PREFIX, LEAGUE_TIRES, PRICE_PREM, PREM_DAYS
)
from system.tires import TIRES
from utils.helpers import get_correct_value, get_index, get_max_fuel
import model
import datetime
from system.levels import LEVELS


class ResponseComposer:
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(ResponseComposer, cls).__new__(cls)
        return cls._instance

    def __init__(self):
        self.k = Keyboard()

    async def _get(self, lang, *, var_name, **kwargs):
        assert isinstance(lang, str)
        return translate(f'misc.{var_name}', locale=lang, **kwargs).expandtabs(2)

    async def _big_number_str(self, value):
        return "{:,}".format(value)

    async def _get_upgrade_user_points(self, user):
        points = user.tires_control + user.straights + user.turns
        return user.lvl - points

    async def first_msg(self, user):
        lang = user.lang
        text = await self._get(lang, var_name='first_msg', nickname=user.username)
        points = await self._get_upgrade_user_points(user)
        k = await self.k.main_menu(lang, points)
        return text, k

    async def parse_username(self, user):
        if user.vip_to > datetime.datetime.utcnow():
            return f"{VIP_ACCOUNT_PREFIX}<b>{user.username}</b>"
        else:
            return f"{PLAIN_ACCOUNT_PREFIX}{user.username}"

    async def profile(self, user, online, car):
        lang = user.lang
        money = await self._big_number_str(user.money) + '$'
        exp = await self._big_number_str(user.exp)
        fuel = get_max_fuel(user)
        nick = await self.parse_username(user)
        text = await self._get(lang, var_name='profile', money=money, fuel=fuel, online=online,
                               left_fuel=user.fuel, nickname=nick, lvl=user.lvl, exp=exp, car=car.base,
                               league=CAR_LVL_SM[car.lvl])
        k = await self.k.profile(lang)
        return text, k

    async def full_profile(self, user, can_upgrade, online, car):
        lang = user.lang
        money = await self._big_number_str(user.money) + '$'
        exp = await self._big_number_str(user.exp)
        to_next_lvl = await self._big_number_str(LEVELS[user.lvl+1] - user.exp)
        fuel = get_max_fuel(user)
        nick = await self.parse_username(user)
        text = await self._get(lang, var_name='full_profile', money=money, fuel=fuel, to_next_lvl=to_next_lvl,
                               left_fuel=user.fuel, nickname=nick, lvl=user.lvl, exp=exp, speed=user.straights,
                               tires_control=user.tires_control, turns=user.turns, online=online, car=car.base,
                               league=CAR_LVL_SM[car.lvl])
        k = await self.k.full_profile(lang, can_upgrade)
        return text, k

    async def affiliate(self, user, ref_cnt, ref_earnings):
        lang = user.lang
        earnings = await self._big_number_str(ref_earnings) + '$'
        if ref_cnt:
            result = await self._get(lang, var_name='your_referrals')
            all_refs_strings = []
            for lvl, cnt in sorted(ref_cnt.items()):
                ref_lvl_str = await self._get(lang, var_name='ref_stat_by_lvl', lvl=lvl, cnt=cnt)
                all_refs_strings.append(ref_lvl_str)
            result += '\n\t'
            result += '\n\t'.join(all_refs_strings)
            ref_cnt_str = result.expandtabs(tabsize=2)
        else:
            ref_cnt_str = ''
        text = await self._get(lang, var_name='affiliate', ref_cnt_str=ref_cnt_str, earnings=earnings)

        k = await self.k.get_ref_link(lang)

        return text, k

    async def get_ref_link(self, user, bot_name):
        return f't.me/{bot_name}?start={user.ref_link}', None

    async def new_referral(self, referred_from, referral, level):
        percent = get_correct_value(REFERRAL_INCOME[level-1] * 100)
        text = await self._get(referred_from.lang, var_name='new_referral',
                               ref_name=referral.username, level=level, percent=percent)
        k = await self.k.invite_more(referred_from.lang)
        return text, k

    async def get_user(self, user, target_user, target_user_stat):
        lang = user.lang
        if target_user:
            registration_date = str(target_user.created_at.date())
            nick = await self.parse_username(target_user)
            text = await self._get(lang, var_name='get_user', races=target_user_stat.races, username=nick,
                                   reg_date=registration_date)
        else:
            text = await self._get(lang, var_name='user_not_found')

        k = None

        return text, k

    async def communication(self, user):
        lang = user.lang
        text = await self._get(lang, var_name='communication')
        k = await self.k.communication_links(lang)
        return text, k

    async def _parse_car(self, lang, car):
        price = await self._big_number_str(car['price'])
        max_speed = get_index(car['max_speed'])
        acceleration = get_index(car['acceleration'])
        consumption = get_index(car['consumption'])
        return await self._get(lang, var_name='car_shop', name=car['name'], max_speed=max_speed,
                               acceleration=acceleration, price=price, consumption=consumption)

    async def market(self, user):
        lang = user.lang
        text = await self._get(lang, var_name='market')
        k = await self.k.market(lang)
        return text, k

    async def donations(self, user):
        lang = user.lang
        prices = [f'{price} {symbol.upper()}' for symbol, price in PRICE_PREM.items()]
        text = await self._get(lang, var_name='donations', donation_price=', '.join(prices))
        k = await self.k.donations(lang)
        return text, k

    async def donation_accepted(self, user):
        lang = user.lang
        text = await self._get(lang, var_name='donation_accepted', days=PREM_DAYS)
        k = None
        return text, k

    async def salon(self, user):
        lang = user.lang
        text = await self._get(lang, var_name='salon')
        k = await self.k.salon(lang)
        return text, k

    async def salon_by_lvl(self, user, cars, lvl):
        lang = user.lang
        cars_str = '\n\n'.join([await self._parse_car(lang, car) for car in cars])
        text = await self._get(lang, var_name='salon_by_lvl', lvl=lvl, cars_str=cars_str)
        k = await self.k.get_cars(lang, cars, lvl)
        return text, k

    async def car_in_shop(self, user, car, is_in_garage, car_lvl, car_index):
        lang = user.lang
        text = await self._parse_car(lang, car)
        can_buy = user.money >= car['price']
        k = await self.k.car_in_shop(lang, is_in_garage, car_lvl, car_index, can_buy)
        return text, k

    async def this_car_in_garage(self, user, car):
        lang = user.lang
        text = await self._get(lang, var_name='car_already_in_garage', name=car['name'])
        k = None
        return text, k

    async def not_enough_money_buy_car(self, user, car):
        lang = user.lang
        text = await self._get(lang, var_name='not_enough_money_buy_car', name=car['name'])
        k = None
        return text, k

    async def bought_car(self, user, car):
        lang = user.lang
        text = await self._get(lang, var_name='bought_car', name=car.name)
        k = await self.k.bought_car(lang)
        return text, k

    async def _parse_garage_car(self, lang, car: model.UserCar):
        if car.is_chosen:
            chosen_sm = '✅️'
        else:
            chosen_sm = '️'

        max_speed = get_index(car.max_speed)
        acceleration = get_index(car.acceleration)
        consumption = get_index(car.consumption)
        if car.name != car.base:
            name = f'{car.name} ({car.base})'
        else:
            name = car.name
        return await self._get(lang, var_name='garage_car', chosen_sm=chosen_sm, max_speed=max_speed, name=name,
                               acceleration=acceleration, consumption=consumption)

    async def _parse_garage_cars(self, lang, cars):
        cars_str = []
        for car in cars:
            t = await self._parse_garage_car(lang, car)
            cars_str.append(t)
        return cars_str

    async def garage(self, user):
        lang = user.lang
        text = await self._get(lang, var_name='garage')
        k = await self.k.garage(lang)
        return text, k

    async def spares_garage(self, user):
        lang = user.lang
        text = await self._get(lang, var_name='spares_garage', sfc_cnt=user.small_fuel_canister,
                               knifes=user.keys, boxes=user.lootboxes)
        k = await self.k.spares_garage(lang, user)
        return text, k

    async def autos(self, user, cars):
        lang = user.lang
        cars_str_list = await self._parse_garage_cars(lang, cars)
        cars_str = '\n\n'.join(cars_str_list)
        text = await self._get(lang, var_name='autos', cars=cars_str)
        k = await self.k.autos(lang, cars)
        return text, k

    async def car_in_garage(self, user, car):
        lang = user.lang
        text = await self._parse_garage_car(lang, car)
        is_vip = user.vip_to > datetime.datetime.utcnow()
        k = await self.k.car_in_garage(lang, car, is_vip)
        return text, k

    async def race(self, user, in_queue, in_races, next_race_time, league, car_name, car_base, this_hour_track):
        lang = user.lang
        minutes_to_next_race = (next_race_time - datetime.datetime.utcnow() + datetime.timedelta(minutes=1)).seconds//60 + 1
        is_part_sm = ACTIVE_OR_NOT_SM[int(user.is_participating)]
        in_queue_str = ''
        this_hour_track = f'{this_hour_track["flag"]} <b>{this_hour_track["name"]}</b>'
        for item in in_queue:
            in_queue_str += f'\t{CAR_LVL_SM[item.lvl]}: <b>{item.cnt}</b>\n'.expandtabs(2)
        if car_name != car_base:
            car_name = f'{car_name} ({car_base})'
        text = await self._get(lang, var_name='race', part_sm=is_part_sm, in_queue=in_queue_str, track=this_hour_track,
                               mins_to_race=minutes_to_next_race, in_races=in_races, car=car_name, league=CAR_LVL_SM[league])
        k = await self.k.race(lang, user.is_participating)
        return text, k

    async def ref_earn(self, referred_from, referral, value, lvl):
        lang = referred_from.lang
        text = await self._get(lang, var_name='ref_earn', ref=referral.username,
                               val=await self._big_number_str(value), lvl=lvl)
        k = await self.k.invite_more(lang)
        return text, k

    async def no_cars(self, user):
        lang = user.lang
        text = await self._get(lang, var_name='no_cars')
        k = await self.k.inline_market(lang)
        return text, k

    async def speed_symbol(self, user):
        return await self._get(user.lang, var_name='speed_symbol')

    async def _get_percentage(self, full_value, current_value):
        return round(current_value/full_value*100)

    async def race_short_info(self, lang, country, autodrome, lap, laps, racer: model.UserRacer, league):
        text = await self._get(lang, var_name='race_short_info', flag=COUNTRY_FLAGS[country],
                               autodrome=autodrome, lap=lap, laps=laps)
        if len(LEAGUE_TIRES[league]) > 1:
            percentage = await self._get_percentage(TIRES[racer.tires]['durability'], racer.tires_durability)
            text += await self._get(lang, var_name='tires_race', perc=percentage, tires=racer.tires)
        else:
            text += '\n'
        return text

    async def not_enough_fuel(self, user, max_fuel):
        lang = user.lang
        text = await self._get(lang, var_name='not_enough_fuel', need=max_fuel, fuel=user.fuel)
        k = None
        return text, k

    async def home(self, user, stat: model.Statistics):
        lang = user.lang
        text = await self._get(lang, var_name='home', gold=stat.gold, silver=stat.silver, bronze=stat.bronze, races=stat.races)
        k = await self.k.home(lang)
        return text, k

    async def settings(self, user):
        lang = user.lang
        ref_notify_status = ACTIVE_OR_NOT_SM[bool(user.ref_notify)]
        text = await self._get(lang, var_name='settings', cur_lang=LANG_FLAGS[lang],
                               lang_code=lang.upper(), ref_notify=ref_notify_status, nick=user.username)
        is_vip = user.vip_to > datetime.datetime.utcnow()
        k = await self.k.settings(lang, is_vip)
        return text, k

    async def change_nickname(self, user):
        lang = user.lang
        text = await self._get(lang, var_name='change_nickname')
        k = await self.k.cancel(lang)
        return text, k

    async def wrong_nickname(self, user):
        lang = user.lang
        text = await self._get(lang, var_name='wrong_nickname')
        k = await self.k.cancel(lang)
        return text, k

    async def done(self, user):
        lang = user.lang
        text = await self._get(lang, var_name='done')
        points = await self._get_upgrade_user_points(user)
        k = await self.k.main_menu(lang, points)
        return text, k

    async def canceled(self, user):
        lang = user.lang
        text = await self._get(lang, var_name='canceled')
        points = await self._get_upgrade_user_points(user)
        k = await self.k.main_menu(lang, points)
        return text, k

    async def ratings(self, user):
        lang = user.lang
        text = await self._get(lang, var_name='ratings')
        k = await self.k.ratings(lang)
        return text, k

    async def richest(self, user, users, user_place, user_obj, user_teamlogo):
        lang = user.lang
        text = ''
        for i, (u, team) in enumerate(users[:USERS_IN_TOP_RATING], 1):
            money = await self._big_number_str(u.money)
            nick = await self.parse_username(u)
            text += f'<b>{i}.</b> {nick} {team}\n\t💰 {money}$\n\n'.expandtabs(tabsize=2)
        if user_place > USERS_IN_TOP_RATING:
            money = await self._big_number_str(user_obj.money)
            nick = await self.parse_username(user)
            text += f'...\n\n<b>{user_place}.</b> {nick} {user_teamlogo}\n\t💰 {money}$\n\n'.expandtabs(tabsize=2)
        k = await self.k.back_to_ratings(lang)
        return text, k


    async def friendly(self, user, users, user_place, user_obj, user_teamlogo):
        lang = user.lang
        text = ''
        for i, (u, team) in enumerate(users[:USERS_IN_TOP_RATING], 1):
            nick = await self.parse_username(u)
            text += f'<b>{i}.</b> {nick} {team}\n\t👔 {u.cnt}\n\n'.expandtabs(tabsize=2)
        if user_place and user_place > USERS_IN_TOP_RATING:
            nick = await self.parse_username(user)
            text += f'...\n\n<b>{user_place}.</b> {nick} {user_teamlogo}\n\t👔 {user_obj.cnt}\n\n'.expandtabs(tabsize=2)
        k = await self.k.back_to_ratings(lang)
        return text, k

    async def truckers(self, user, users, user_place, user_obj, user_teamlogo):
        lang = user.lang
        text = ''
        for i, (u, team) in enumerate(users[:USERS_IN_TOP_RATING], 1):
            mileage = await self._big_number_str(int(u.mileage))
            nick = await self.parse_username(u)
            text += f'<b>{i}.</b> {nick} {team}\n\t🚚 {mileage} km\n\n'.expandtabs(tabsize=2)
        if user_place and user_place > USERS_IN_TOP_RATING:
            mileage = await self._big_number_str(int(user_obj.mileage))
            nick = await self.parse_username(user)
            text += f'...\n\n<b>{user_place}.</b> {nick} {user_teamlogo}\n\t🚚 {mileage} km\n\n'.expandtabs(tabsize=2)
        k = await self.k.back_to_ratings(lang)
        return text, k

    async def fastests(self, user, laps, user_place, user_obj):
        lang = user.lang
        text = ''
        for i, u in enumerate(laps[:USERS_IN_TOP_RATING], 1):
            speed = u.speed
            nick = await self.parse_username(u)
            text += f'<b>{i}.</b> {nick}\n\t🛣 {u.autodrome}\n\t💨 {speed} km/h\n\n'.expandtabs(tabsize=2)
        if user_place and user_place > USERS_IN_TOP_RATING:
            speed = round(user_obj.speed)
            nick = await self.parse_username(user)
            text += f'...\n\n<b>{user_place}.</b> {nick}\n\t🛣 {user_obj.autodrome}\n\t💨 {speed} km/h\n\n'.expandtabs(tabsize=2)
        k = await self.k.back_to_ratings(lang)
        return text, k

    async def medalists(self, user, users, user_place, user_obj, user_teamlogo):
        lang = user.lang
        text = ''
        for i, (u, team) in enumerate(users[:USERS_IN_TOP_RATING], 1):
            nick = await self.parse_username(u)
            text += f'<b>{i}.</b> {nick} {team}\n\t🥇 {u.gold}\n\t🥈 {u.silver}\n\t🥉 {u.bronze}\n\n'.expandtabs(tabsize=2)
        if user_place and user_place > USERS_IN_TOP_RATING:
            nick = await self.parse_username(user)
            text += f'...\n\n<b>{user_place}.</b> {nick} {user_teamlogo}\n\t🥇 {user_obj.gold}\n\t🥈 {user_obj.silver}\n\t🥉 {user_obj.bronze}\n\n'.expandtabs(
                tabsize=2)
        k = await self.k.back_to_ratings(lang)
        return text, k

    async def experienced(self, user, users, user_place, user_obj, user_teamlogo):
        lang = user.lang
        text = ''
        for i, (u, team) in enumerate(users[:USERS_IN_TOP_RATING], 1):
            nick = await self.parse_username(u)
            text += f'<b>{i}.</b> {nick} {team}\n\t🔸 {u.exp}\n\t🎫 {u.lvl}\n\n'.expandtabs(tabsize=2)
        if user_place and user_place > USERS_IN_TOP_RATING:
            nick = await self.parse_username(user)
            text += f'...\n\n<b>{user_place}.</b> {nick} {user_teamlogo}\n\t🔸 {user_obj.exp}\n\t🎫 {user_obj.lvl}\n\n'.expandtabs(tabsize=2)
        k = await self.k.back_to_ratings(lang)
        return text, k

    async def enduring(self, user, users, user_place, user_obj, user_teamlogo):
        lang = user.lang
        text = ''
        for i, (u, team) in enumerate(users[:USERS_IN_TOP_RATING], 1):
            nick = await self.parse_username(u)
            text += f'<b>{i}.</b> {nick} {team}\n\t🛣 {u.laps}\n\n'.expandtabs(tabsize=2)
        if user_place and user_place > USERS_IN_TOP_RATING:
            nick = await self.parse_username(user)
            text += f'...\n\n<b>{user_place}.</b> {nick} {user_teamlogo}\n\t🛣 {user_obj.laps}\n\n'.expandtabs(tabsize=2)
        k = await self.k.back_to_ratings(lang)
        return text, k

    async def teams(self, user, teams):
        lang = user.lang
        text = ''
        for i, team in enumerate(teams, 1):
            text += f'<b>{i}.</b> {team.logo} <b>{team.name}</b>\n\t🔹 {team.points}\n\t👥 <b>{team.cnt}</b>\n\n'.expandtabs(tabsize=2)
        k = await self.k.back_to_ratings(lang)
        return text, k

    async def race_info(self, racer, race):
        lang = racer.user.lang
        distance = get_correct_value(round(race.laps * race.lap_len / 1000, 2))
        speed_index = get_index(race.speed_index)
        text = await self._get(lang, var_name='race_info', autodrome=race.autodrome, flag=COUNTRY_FLAGS[race.country],
                               laps=race.laps, lap_len=race.lap_len, distance=distance, turns=race.turns,
                               straights=race.straights, speed_index=speed_index)
        if len(LEAGUE_TIRES[race.league]) > 1:
            k = await self.k.tires_race(LEAGUE_TIRES[race.league], racer.id)
        else:
            k = None
        return text, k

    async def info_track(self, user, autodrome):
        lang = user.lang
        speed_index = get_index(autodrome['speed_index'])
        text = await self._get(lang, var_name='info_track', autodrome=autodrome['name'],
                               flag=COUNTRY_FLAGS[autodrome['country']], lap_len=autodrome['length'],
                               turns=autodrome['turns'], straights=autodrome['straights'], speed_index=speed_index)
        k = None
        return text, k

    async def get_racer_info(self, leader, racer, place, lap, league, last_lap, get_correct_time):
        time = get_correct_time(racer.main_time, leader.main_time, place)
        nick = await self.parse_username(racer.user)
        team_logo = getattr(racer.team, 'logo', '')
        car_name = racer.car.name if racer.car.name == racer.car.base else f'{racer.car.name} ({racer.car.base})'
        text = f'<b>{place}.</b> {nick} {team_logo}\n' \
               f'\t🎫 <b>{racer.user.lvl}</b>\n' \
               f'\t🚘 {car_name}\n' \
               f'\t⏱ {time}\n'
        if lap > 0:
            text += f'\t💨 {round(last_lap.speed, 2)} km/h\n'
        if len(LEAGUE_TIRES[league]) > 1:
            text += f'\t🔘 <b>{racer.tires}</b>\n\n'
        else:
            text += '\n'

        return text

    async def get_tires_kb_race(self, racer, league):
        if len(LEAGUE_TIRES[league]) > 1:
            k = await self.k.tires_race(LEAGUE_TIRES[league], racer.id)
        else:
            k = None
        return k

    async def graduation_text(self, user, winners, race):
        lang = user.lang
        winners_text = ''
        for place, racer in enumerate(winners, 1):
            nick = await self.parse_username(racer.user)
            money = MONEY_FOR_RACE[place] * LEAGUE_BUSTS[race.league]
            if user.vip_to > datetime.datetime.utcnow():
                money *= 2
            if place < 4:
                winners_text += f'{WINNERS_MEDALS[place-1]} {nick} {getattr(racer.team, "logo", "")}\n' \
                                f'\t💰 {money}$\n' \
                                f'\t🔸 {EXP_FOR_PLACE[place] * LEAGUE_BUSTS[race.league]}\n\n'.expandtabs(tabsize=2)
            else:
                winners_text += f'{nick} {getattr(racer.team, "logo", "")}\n' \
                                f'\t💰 {money}$\n'.expandtabs(tabsize=2)
        text = await self._get(lang, var_name='graduation_race', autodrome=race.autodrome, flag=COUNTRY_FLAGS[race.country],
                               winners_text=winners_text)
        points = await self._get_upgrade_user_points(user)
        k = await self.k.main_menu(lang, points)
        return text, k

    async def medal_text(self, user, place):
        lang = user.lang
        text = await self._get(lang, var_name='medal', place=place, medal=WINNERS_MEDALS[place-1])
        return text

    async def lootbox_text(self, user):
        lang = user.lang
        text = await self._get(lang, var_name='lootbox', lootbox=LOOTBOX)
        return text

    async def key_text(self, user):
        lang = user.lang
        text = await self._get(lang, var_name='key', k=KEY)
        return text

    async def get_home_kb(self, user):
        lang = user.lang
        k = await self.k.medal_home(lang)
        return k

    async def new_canister(self, user):
        lang = user.lang
        text = await self._get(lang, var_name='new_canister')
        k = await self.k.use_canister(lang)
        return text, k

    async def error(self, user, main_menu=False):
        lang = user.lang
        text = await self._get(lang, var_name='error')
        if main_menu:
            points = await self._get_upgrade_user_points(user)
            k = await self.k.main_menu(lang, points)
        else:
            k = None
        return text, k

    async def get_k_send_canister(self, target_user):
        return await self.k.send_canister(target_user.id)

    async def best_tracks_for_car(self, user, autodromes):
        lang = user.lang
        ad_text = ', '.join(autodromes)
        text = await self._get(lang, var_name='passed_autodromes', ads=ad_text)
        return text, None

    async def no_team(self, user, can_create_team):
        lang = user.lang
        req_money = await self._big_number_str(TEAM_REQ_MONEY)
        text = await self._get(lang, var_name='no_team', req_money=req_money, req_lvl=TEAM_REQ_LVL)
        k = await self.k.no_team(lang, can_create_team)
        return text, k

    async def team(self, user, team, racers):
        lang = user.lang
        team_money = await self._big_number_str(team.money)
        team_name = f'{team.logo} <b>{team.name}</b>'
        text = await self._get(lang, var_name='team', name=team_name, money=team_money, pnts=team.points,
                               ws=get_index(team.weather_station), gs=get_index(team.gas_station), building_points=team.building_points,
                               office=get_index(team.head_office), racers=racers)
        k = await self.k.team(lang, team)
        return text, k

    async def create_team(self, user):
        lang = user.lang
        text = await self._get(lang, var_name='create_team')
        k = await self.k.cancel(lang)
        return text, k

    async def process_team_name(self, user, team_name):
        lang = user.lang
        text = await self._get(lang, var_name='process_team_name', team_name=team_name)
        k = await self.k.cancel(lang)
        return text, k

    async def office(self, user, team):
        lang = user.lang
        next_lvl_money = await self._big_number_str(TEAM_BUILDINGS_PRICE_MONEY[team.head_office+1])
        next_lvl_points = await self._big_number_str(TEAM_BUILDINGS_PRICE_HUMANHOURS[team.head_office+1])
        text = await self._get(lang, var_name='office', office=get_index(team.head_office), managers=team.managers,
                               mechanics=team.mechanics, builders=team.builders, next_lvl_cost=next_lvl_money,
                               next_lvl_points=next_lvl_points)
        k = await self.k.office(lang, user, team, TEAM_BUILDINGS_PRICE_MONEY[team.head_office+1],
                                TEAM_BUILDINGS_PRICE_HUMANHOURS[team.head_office+1])
        return text, k

    async def gas_station(self, user, team):
        lang = user.lang
        next_lvl_money = await self._big_number_str(TEAM_BUILDINGS_PRICE_MONEY[team.gas_station+1])
        next_lvl_points = await self._big_number_str(TEAM_BUILDINGS_PRICE_HUMANHOURS[team.gas_station+1])
        ref_speed = REFUELING_SPEED[team.gas_station]
        if team.weather_station == 0:
            text = await self._get(lang, var_name='gas_station_build',
                                   build_cost=next_lvl_money, build_points=next_lvl_points)
        else:
            text = await self._get(lang, var_name='gas_station', gs=get_index(team.gas_station), next_lvl_cost=next_lvl_money,
                                   next_lvl_points=next_lvl_points, refueling_speed=ref_speed)
        k = await self.k.gas_station(lang, user, team, TEAM_BUILDINGS_PRICE_MONEY[team.gas_station+1],
                                     TEAM_BUILDINGS_PRICE_HUMANHOURS[team.gas_station+1])
        return text, k

    async def weather_station(self, user, team):
        lang = user.lang
        next_lvl_money = await self._big_number_str(TEAM_BUILDINGS_PRICE_MONEY[team.weather_station+1])
        next_lvl_points = await self._big_number_str(TEAM_BUILDINGS_PRICE_HUMANHOURS[team.weather_station+1])
        meteo_power = METEO_POWER[team.weather_station]
        if team.weather_station == 0:
            text = await self._get(lang, var_name='weather_station_build',
                                   build_cost=next_lvl_money, build_points=next_lvl_points)
        else:
            text = await self._get(lang, var_name='weather_station', ws=get_index(team.weather_station),
                                   next_lvl_cost=next_lvl_money, next_lvl_points=next_lvl_points, meteo_power=meteo_power)
        k = await self.k.weather_station(lang, user, team, TEAM_BUILDINGS_PRICE_MONEY[team.weather_station + 1],
                                         TEAM_BUILDINGS_PRICE_HUMANHOURS[team.weather_station + 1])
        return text, k

    async def not_enough_money(self, user):
        lang = user.lang
        text = await self._get(lang, var_name='not_enough_money')
        k = None
        return text, k

    async def team_info(self, user, team, lvl_limit, lvls_sum, racers_in_team):
        lang = user.lang
        creation_date = team.created_at.strftime("%d.%m.%y")
        text = await self._get(lang, var_name='team_info', racers=racers_in_team, lvl_sum=lvls_sum, lvl_limit=lvl_limit,
                               name=team.name, logo=team.logo, lvl=team.head_office, creation_date=creation_date)
        k = None
        return text, k

    async def offer_by_forward(self, user, target_user, cars_cnt, team, is_team_limited):
        lang = user.lang
        if team is None:
            team_name = '🚫'
        else:
            team_name = team.name
        nick = await self.parse_username(target_user)
        text = await self._get(lang, var_name='user_info_to_director', nickname=nick, lvl=target_user.lvl,
                               cars_cnt=cars_cnt, team=team_name)
        if is_team_limited or team is not None:
            k = None
        else:
            k = await self.k.offer_by_forward(lang, target_user)
        return text, k

    async def contract_offer(self, user):
        lang = user.lang
        text = await self._get(lang, var_name='contract_offer', min_len=MIN_CONTRACT_LEN, max_len=MAX_CONTRACT_LEN)
        k = await self.k.cancel(lang)
        return text, k

    async def new_offer(self, user, contract):
        lang = user.lang
        valid_to = contract.valid_to.strftime("%d.%m.%y")
        text = await self._get(lang, var_name='new_offer', team=contract.team.name, valid_to=valid_to)
        k = await self.k.sign_contract(lang, contract.id)
        return text, k

    async def contract_signed(self, user, target_user, contract):
        lang = user.lang
        valid_to = contract.valid_to.strftime("%d.%m.%y")
        nick = await self.parse_username(target_user)
        text = await self._get(lang, var_name='new_contract', nickname=nick, valid_to=valid_to)
        k = None
        return text, k

    async def contract_info(self, user, contract):
        lang = user.lang
        valid_to = contract.valid_to.strftime("%d.%m.%y %H:%M")
        text = await self._get(lang, var_name='contract_info', valid_to=valid_to)
        k = None
        return text, k

    async def contracts(self, user, contracts):
        lang = user.lang
        result = ''
        for contract in contracts:
            valid_to = contract.valid_to.strftime("%d.%m.%y %H:%M")
            result += await self._get(lang, var_name='contract_info_director', valid_to=valid_to, racer=contract.racer.username)
            result += '\n'
        k = None
        return result, k

    async def team_won_daily(self, user, place, earning, team):
        lang = user.lang
        earning = await self._big_number_str(earning)
        team_name = f'{team.logo} <b>{team.name}</b>'
        text = await self._get(lang, var_name='team_won_daily', earning=earning, place=CAR_LVL_SM[place], team=team_name)
        return text

    async def fuel_full(self, user):
        lang = user.lang
        text = await self._get(lang, var_name='fuel_full')
        k = await self.k.fuel_full(lang)
        return text, k

    async def get_opened_box_win(self, user, thing):
        lang = user.lang
        text = await self._get(lang, var_name=f'won_{thing.lower()}')
        return text


rc = ResponseComposer()
