import peewee
import peewee_async
from system.settings import START_FUEL, START_MONEY, loop
from system.tires import TIRES
import os
from datetime import datetime
from playhouse.migrate import *
from playhouse.postgres_ext import *
from utils.helpers import get_ref_link


name = os.environ.get('DB_NAME')
user = os.environ.get('DB_USER')
host = os.environ.get('DB_HOST')
port = os.environ.get('DB_PORT')
passwd = os.environ.get('DB_PASSWORD')


database = peewee_async.PooledPostgresqlDatabase(name, host=host, port=int(port), user=user, password=passwd)
migrator = PostgresqlMigrator(database)


class BaseModel(peewee.Model):
    class Meta:
        database = database


class User(BaseModel):
    telegram_id = peewee.IntegerField(unique=True)
    money = peewee.BigIntegerField(default=START_MONEY)
    fuel = peewee.IntegerField(default=START_FUEL)
    created_at = peewee.DateTimeField(default=datetime.utcnow)
    is_deleted = peewee.BooleanField(default=False)
    is_baned = peewee.BooleanField(default=False)
    username = peewee.TextField()
    lang = peewee.CharField(default='ru')
    ref_link = peewee.TextField(default=get_ref_link)
    status = peewee.CharField(default='rest')

    lvl = peewee.SmallIntegerField(default=1)
    exp = peewee.BigIntegerField(default=0)

    ref_notify = peewee.BooleanField(default=True)

    small_fuel_canister = peewee.SmallIntegerField(default=0)
    lootboxes = peewee.SmallIntegerField(default=0)
    keys = peewee.SmallIntegerField(default=0)

    vip_to = DateTimeField(default=datetime.utcnow)

    tires_control = SmallIntegerField(default=0)
    straights = SmallIntegerField(default=0)
    turns = SmallIntegerField(default=0)

    is_participating = peewee.BooleanField(default=False)
    is_admin = peewee.BooleanField(default=False)


class Statistics(BaseModel):
    user = peewee.ForeignKeyField(User)
    races = peewee.IntegerField(default=0)
    earned_from_ref = peewee.IntegerField(default=0)
    gold = peewee.IntegerField(default=0)
    silver = peewee.IntegerField(default=0)
    bronze = peewee.IntegerField(default=0)


class UserParticipation(BaseModel):
    user = peewee.ForeignKeyField(User, backref='participation')
    is_participating = peewee.BooleanField(default=False)


class Referrals(BaseModel):
    referral = peewee.ForeignKeyField(User)
    referred_from = peewee.ForeignKeyField(User, backref='referrals')
    level = peewee.SmallIntegerField(default=1)


class UserCar(BaseModel):
    user = peewee.ForeignKeyField(User, backref='cars')

    name = peewee.CharField()
    base = peewee.CharField()
    max_speed = peewee.SmallIntegerField()
    acceleration = peewee.SmallIntegerField()
    consumption = peewee.SmallIntegerField()
    lvl = peewee.SmallIntegerField()

    is_chosen = peewee.BooleanField(default=False)

    created_at = peewee.DateTimeField(default=datetime.utcnow)


class Team(BaseModel):
    name = CharField()
    logo = CharField()
    director = ForeignKeyField(User)
    chat_link = CharField(default='')

    points = IntegerField(default=0)
    money = BigIntegerField(default=0)
    building_points = IntegerField(default=0)

    managers = SmallIntegerField(default=0)
    mechanics = SmallIntegerField(default=0)
    builders = SmallIntegerField(default=0)

    weather_station = SmallIntegerField(default=0)
    gas_station = SmallIntegerField(default=0)
    head_office = SmallIntegerField(default=1)

    created_at = DateTimeField(default=datetime.utcnow)
    is_deleted = BooleanField(default=False)


class Race(BaseModel):
    country = peewee.CharField(default='')
    autodrome = peewee.TextField()
    created_at = peewee.DateTimeField(default=datetime.utcnow)
    laps = peewee.IntegerField()
    lap_len = peewee.IntegerField()
    turns = peewee.SmallIntegerField()
    straights = peewee.SmallIntegerField()
    speed_index = peewee.SmallIntegerField()
    league = peewee.SmallIntegerField()


class Lap(BaseModel):
    number = peewee.SmallIntegerField()
    race = peewee.ForeignKeyField(Race)


class UserRacer(BaseModel):
    user = peewee.ForeignKeyField(User)
    race = peewee.ForeignKeyField(Race, backref='racers')
    car = peewee.ForeignKeyField(UserCar)
    main_time = peewee.DecimalField(default=0, decimal_places=4, max_digits=10)
    message_id = peewee.IntegerField(default=0)
    team = ForeignKeyField(Team, null=True, default=None)
    tires = CharField(default='M')
    chosen_tires = CharField(default='M')
    tires_durability = SmallIntegerField(default=TIRES['M']['durability'])
    box_next_lap = BooleanField(default=False)


class UserLapTime(BaseModel):
    racer = peewee.ForeignKeyField(UserRacer, backref='lap_times')
    lap = peewee.ForeignKeyField(Lap)
    time = peewee.DecimalField(decimal_places=4, max_digits=10)
    speed = peewee.DecimalField(decimal_places=2, max_digits=10)


class Contract(BaseModel):
    team = ForeignKeyField(Team, backref='contracts')
    racer = ForeignKeyField(User, backref='contracts')
    created_at = DateTimeField(default=datetime.utcnow)
    valid_to = DateTimeField()
    signed = BooleanField(default=False)


class Payment(BaseModel):
    user = ForeignKeyField(User, backref='payments')
    payment_id = CharField()
    confirmed = BooleanField(default=False)
    created_at = DateTimeField(default=datetime.utcnow)


class Clicks(BaseModel):
    user = ForeignKeyField(User, backref='clicks')
    target = CharField()
    date = DateTimeField(default=datetime.utcnow)


tables = [User, Statistics, UserParticipation, Referrals, UserCar, Team, Race, Lap, UserRacer, UserLapTime,
          Contract, Clicks]
database.create_tables(tables, safe=True)


objects = peewee_async.Manager(database, loop=loop)
# loop.run_until_complete(objects.connect())
