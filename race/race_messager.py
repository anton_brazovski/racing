from model import *
from race.race_helpers import RaceHelpers
from system.settings import bot
from response_composer import rc
import asyncio


class RaceMessager:

    @staticmethod
    async def get_lap_info(lap, race, racers, racer):
        text = await rc.race_short_info('en', race.country, race.autodrome, lap, race.laps, racer, race.league)

        for i, racer_ in enumerate(racers, 1):
            try:
                last_lap = await objects.get(racer_.lap_times.join(Lap).where((Lap.number == lap) & (Lap.race == race)))
            except:
                last_lap = None
            text += await rc.get_racer_info(racers[0], racer_, i, lap, race.league, last_lap, RaceHelpers.get_correct_time)

        k = await rc.get_tires_kb_race(racer, race.league)

        return text, k

    @staticmethod
    async def send_lap_info(lap, race, racers):

        for racer in racers:
            text, k = await RaceMessager.get_lap_info(lap, race, racers, racer)
            try:
                if racer.message_id == 0:
                    msg = await bot.send_message(chat_id=racer.user.telegram_id, text=text, reply_markup=k)
                    racer.message_id = msg.message_id
                    await objects.update(racer)
                else:
                    await bot.edit_message_text(chat_id=racer.user.telegram_id, text=text, message_id=racer.message_id,
                                                reply_markup=k)
            except:
                continue

    @staticmethod
    async def send_race_info(race, racers):
        tasks = []
        for racer in racers:
            text, k = await rc.race_info(racer, race)
            tasks.append(RaceMessager.send_msg(t_id=racer.user.telegram_id, text=text, k=k))
        await asyncio.gather(*tasks)

    @staticmethod
    async def msg_ref_earn(referred_from, referral, value, lvl):
        text, k = await rc.ref_earn(referred_from, referral, value, lvl)
        try:
            await bot.send_message(text=text, chat_id=referred_from.telegram_id, reply_markup=k)
        except Exception as e:
            logger.warn(f'Message about referral deal not sent to user {referred_from.id}: {e}')

    @staticmethod
    async def send_msg(text, t_id, k=None):
        try:
            await bot.send_message(chat_id=t_id, text=text, reply_markup=k)
        except Exception as e:
            logger.warn(f'Msg in race not sent: {e}')

    @staticmethod
    async def send_graduation(u, place, race, won_racers, with_box, with_key):
        text, k = await rc.graduation_text(u, won_racers, race)
        await RaceMessager.send_msg(text, u.telegram_id, k=k)
        loot_text = ''
        if place < 4:
            loot_text += await rc.medal_text(u, place)
        if with_box:
            loot_text += '\n\n'
            loot_text += await rc.lootbox_text(u)
        if with_key:
            loot_text += '\n\n'
            loot_text += await rc.key_text(u)

        if loot_text:
            k = await rc.get_home_kb(u)
            await RaceMessager.send_msg(loot_text, u.telegram_id, k=k)

