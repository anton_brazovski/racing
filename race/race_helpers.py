import random
from model import objects, Contract, Team, UserRacer, DoesNotExist
from system.settings import (BASE_PITSTOP, BASE_SPEED, BASE_SPEED_RAND_DIFF, BASE_TEAM_LVL_LIMIT, MECHANIC_BONUS,
                             SPEED_BONUS_LVL, SPEED_BONUS_LVL_LIMIT, SPEED_BONUS_TRACK, DRIVER_SKILLS_BONUS)
from system.tires import TIRES
from decimal import Decimal
import datetime



class RaceHelpers:

    @staticmethod
    async def _get_percentage(full_value, current_value):
        return round(current_value/full_value*100)

    @classmethod
    async def _get_random_coeff(cls, lvl):
        down = -BASE_SPEED_RAND_DIFF + float(lvl * SPEED_BONUS_LVL)
        up = BASE_SPEED_RAND_DIFF + float(lvl * SPEED_BONUS_LVL_LIMIT)
        random_diff = random.uniform(down, up)
        random_coeff = 1 + random_diff
        return random_coeff

    @classmethod
    async def get_speed(cls, car, race, racer: UserRacer):
        random_coeff = await cls._get_random_coeff(racer.user.lvl)

        track_speed = race.speed_index

        max_speed_bonus_skill = DRIVER_SKILLS_BONUS['straights'] * racer.user.straights
        acc_speed_bonus_skill = DRIVER_SKILLS_BONUS['turns'] * racer.user.turns

        tires_percents = await RaceHelpers._get_percentage(TIRES[racer.tires]['durability'], racer.tires_durability)
        decrese_coeff = round((10 - int(tires_percents / 10)) * 0.15, 2)
        acc_coeff = max(car.acceleration + TIRES[racer.tires]['acceleration'] - decrese_coeff, 1)
        speed_coeff = max(car.max_speed + TIRES[racer.tires]['speed'] - decrese_coeff, 1)

        acc_speed = (acc_coeff/10 + 1 + acc_speed_bonus_skill) * BASE_SPEED
        max_speed = (speed_coeff/10 + 1 + max_speed_bonus_skill) * BASE_SPEED

        acc_coefficient = round((10 - track_speed)/10, 2)  # .01 = %
        max_speed_coefficient = round(1 - acc_coefficient, 2)

        assert acc_coefficient + max_speed_coefficient == 1

        sp = (acc_speed * acc_coefficient) + (max_speed * max_speed_coefficient)
        speed = Decimal(str(sp * random_coeff))
        speed += speed * (Decimal(track_speed) * SPEED_BONUS_TRACK)
        speed = round(speed, 3)
        return speed

    @staticmethod
    async def get_lap_time(racer, speed, race, lap):
        track_len = race.lap_len
        t = round(Decimal(track_len)/speed, 4)
        if racer.box_next_lap and racer.team and lap > 1:
            pit_time = BASE_PITSTOP
            for _ in range(racer.team.mechanics):
                pit_time -= pit_time * MECHANIC_BONUS
            t += Decimal(str(pit_time))
        return t

    @staticmethod
    def get_correct_value(num):
        if '.' in str(num):
            num = str(num).rstrip('0')
            if num[-1] == '.':
                num = num[:-1]
        return num

    @staticmethod
    def get_correct_time(racer_time, leader_time, place):
        diff = racer_time - leader_time
        if place > 1:
            return f'+{RaceHelpers.get_correct_value(diff)} sec.'
        else:
            return f'Leader'

    @staticmethod
    async def get_team(u):
        try:
            c = await objects.get(u.contracts.where(
                Contract.signed & (Contract.valid_to > datetime.datetime.utcnow())
            ))
            team = c.team
        except DoesNotExist:
            try:
                team = await objects.get(Team, director=u)
            except DoesNotExist:
                team = None
        return team

    @staticmethod
    async def is_won(chance, league, from_league):
        if league < from_league:
            return False
        total_percents = 100
        rand_num = random.randint(1, total_percents)
        return rand_num in range(1, 1+chance)

    @staticmethod
    async def is_with_prem(u):
        return u.vip_to > datetime.datetime.utcnow()
