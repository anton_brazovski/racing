from model import *
import random
from system.settings import (EXP_FOR_LAP, RAND_DIFF_EXP, EXP_FOR_PLACE, LAPS, WAIT_BEFORE_RACE, TEAM_COMISSION,
                             LEAGUE_TIRES, LAPS_INTERVALS, LEAGUE_BUSTS, MONEY_FOR_RACE, KEY_CHANCES, LOOTBOX_CHANCE,
                             MEDALS, FUEL_FOR_LVL, POINTS_FROM_LEAGUE, REFERRAL_INCOME, DRIVER_SKILLS_BONUS, POINTS)
from system.tires import TIRES
from utils.logger import logger
import asyncio
from decimal import Decimal
from race.race_helpers import RaceHelpers
from race.race_messager import RaceMessager
from race.autodromes import AUTODROMES
from system.levels import LEVELS


class RegularRace:
    def __init__(self):
        self.race_obj = None

    @classmethod
    async def create(cls, cars, league):
        self = RegularRace()
        await self.init_race(cars, league)
        await self._change_status_stat()
        return self

    async def _change_status_stat(self):
        tasks = []
        for racer in self.race_obj.racers:
            racer.user.status = 'race'
            tasks.append(objects.update(racer.user))
            tasks.append(objects.execute(
                Statistics.update({Statistics.races: Statistics.races + 1}).where(Statistics.user == racer.user)))
        await asyncio.gather(*tasks)
        logger.debug(f'Statuses updated league {self.race_obj.league}')

    async def _init_racer(self, car):
        rand_time = random.randint(0, 5)
        team = await RaceHelpers.get_team(car.user)
        await objects.create(UserRacer, user=car.user, race=self.race_obj, car=car, main_time=rand_time, team=team)

    async def init_race(self, cars, league):
        autodrome = AUTODROMES[datetime.utcnow().hour % 12]
        laps = LAPS[league]
        lap_len = autodrome['length']
        autodrome_name = autodrome['name']
        country = autodrome['country']
        turns = autodrome['turns']
        straights = autodrome['straights']
        speed_index = autodrome['speed_index']
        self.race_obj = await objects.create(Race, autodrome=autodrome_name, country=country, laps=laps, league=league,
                                             lap_len=lap_len, turns=turns, straights=straights, speed_index=speed_index)
        tasks = []
        for car in cars:
            tasks.append(self._init_racer(car))
        await asyncio.gather(*tasks)
        logger.debug(f'New race league {league} inited successfully')

    async def _send_lap_info(self, lap=0):
        racers = await objects.execute(UserRacer.select().where(UserRacer.race == self.race_obj).order_by(UserRacer.main_time))
        await RaceMessager.send_lap_info(lap, self.race_obj, racers)

    async def _process_tires(self):
        if len(LEAGUE_TIRES[self.race_obj.league]) > 1:
            racers = await objects.execute(UserRacer.select().where(UserRacer.race == self.race_obj))
            for racer in racers:
                racer.tires = racer.chosen_tires
                racer.tires_durability = TIRES[racer.tires]['durability']
                await objects.update(racer)

    async def _send_race_info(self):
        racers = await objects.execute(UserRacer.select().where(UserRacer.race == self.race_obj))
        await RaceMessager.send_race_info(self.race_obj, racers)

    async def _update_exp(self, racer):
        exp = EXP_FOR_LAP + random.randint(-RAND_DIFF_EXP, RAND_DIFF_EXP)
        return await self._add_exp(racer.user, exp)

    async def _add_exp(self, u, exp):
        add_exp = exp * LEAGUE_BUSTS[self.race_obj.league]
        u.exp += add_exp
        logger.debug(f'Added {add_exp} exp to user {u.id}')
        if u.exp >= LEVELS[u.lvl+1]:
            u.lvl += 1
            u.fuel = START_FUEL + u.lvl * FUEL_FOR_LVL
            logger.debug(f'User {u.id} lvl up, new lvl = {u.lvl}')
        return await objects.update(u)

    async def decrease_fuel(self, racer):
        logger.debug(f'User {racer.user.id} fuel decreased by {racer.car.consumption}, car={racer.car.base}')
        u = racer.user
        u.fuel -= racer.car.consumption
        await objects.update(u)

    async def handle_box(self, racer: UserRacer):
        if racer.box_next_lap:
            racer.tires = racer.chosen_tires
            racer.box_next_lap = False
            durability_bonus = DRIVER_SKILLS_BONUS['tire_control'] * racer.user.lvl
            racer.tires_durability = TIRES[racer.chosen_tires]['durability'] + durability_bonus
        else:
            racer.tires_durability = max(racer.tires_durability - self.race_obj.speed_index, 0)
        await objects.update(racer)

    async def handle_time(self, racer, lap_time):
        racer.main_time += Decimal(str(lap_time))
        await objects.update(racer)

    async def _update_lap(self, lap):
        l = await objects.create(Lap, number=lap, race=self.race_obj)
        racers = await objects.execute(UserRacer.select().where(UserRacer.race == self.race_obj).order_by(UserRacer.main_time))
        tasks = []
        for racer in racers:
            lap_speed = await RaceHelpers.get_speed(racer.car, self.race_obj, racer)
            lap_time = await RaceHelpers.get_lap_time(racer, lap_speed, self.race_obj, lap)
            await self.handle_box(racer)
            await self.handle_time(racer, lap_time)
            logger.debug(f'User {racer.user.id} updated lap time, speed={lap_speed}, time={lap_time}')
            await objects.create(UserLapTime, racer=racer, lap=l, time=lap_time, speed=lap_speed)
            await self.decrease_fuel(racer)
            await self._update_exp(racer)
        await asyncio.gather(*tasks)

    async def comm_to_referrals(self, u, winned_val):
        referreds = await objects.execute(Referrals.select().where(Referrals.referral == u))
        tasks = []
        for ref in referreds:
            comission = int(winned_val * REFERRAL_INCOME[ref.level-1])
            logger.debug(f'{ref.referral.username} -> {ref.referred_from.username}, {comission}$, lvl={ref.level}')
            tasks.append(objects.execute(User.update({User.money: User.money + comission}).where(User.id == ref.referred_from.id)))
            tasks.append(objects.execute(Statistics.update(
                {Statistics.earned_from_ref: Statistics.earned_from_ref + comission}
            ).where(Statistics.user == ref.referred_from.id)))
            if ref.referred_from.ref_notify:
                tasks.append(RaceMessager.msg_ref_earn(ref.referred_from, ref.referral, comission, ref.level))
        await asyncio.gather(*tasks)

    async def set_statuses_rest(self):
        racers = await objects.execute(
            UserRacer.select().where(UserRacer.race == self.race_obj))
        tasks = []
        for racer in racers:
            racer.user.status = 'rest'
            racer.user.is_participating = False
            tasks.append(objects.update(racer.user))
        await asyncio.gather(*tasks)

    async def is_with_team(self, u):
        try:
            return bool(await objects.get(u.contracts.where(Contract.signed & (Contract.valid_to > datetime.utcnow()))))
        except DoesNotExist:
                return bool(await objects.count(Team.select().where(Team.director == u)))

    async def add_team_money(self, u, money_for_team):
        try:
            c = await objects.get(u.contracts.where(Contract.signed & (Contract.valid_to > datetime.utcnow())))
            team = c.team
        except DoesNotExist:
            team = await objects.get(Team.select().where(Team.director == u))

        team.money += money_for_team
        logger.debug(f'Team {team.name} earned {money_for_team}$ from {u.id}')
        await objects.update(team)

    async def _process_money(self, u, place):
        money_for_race = int(MONEY_FOR_RACE[place]) * LEAGUE_BUSTS[self.race_obj.league]
        if await RaceHelpers.is_with_prem(u):
            money_for_race *= 2
        if await self.is_with_team(u):
            money_for_team = int(money_for_race * TEAM_COMISSION)
            money_for_race -= money_for_team
            await self.add_team_money(u, money_for_team)
        u.money += money_for_race
        await objects.update(u)
        await self.comm_to_referrals(u, money_for_race)

    async def _process_exp(self, u, place):
        await self._add_exp(u, EXP_FOR_PLACE[place])

    async def _process_medals(self, u, place):
        stat = await objects.get(Statistics, user=u)
        cur_cnt = getattr(stat, MEDALS[place])
        setattr(stat, MEDALS[place], cur_cnt + 1)
        await objects.update(stat)

    async def _process_loot(self, u: User, with_box, with_key):
        if with_box:
            u.lootboxes += 1
        if with_key:
            u.keys += 1
        await objects.update(u)

    async def _process_points(self, racer, place):
        points = POINTS.get(place, 0)
        if points != 0:
            team: Team = racer.team
            if team:
                team.points += points
                await objects.update(team)

    async def _graduate_user(self, racer, place, won_racers):
        is_with_lootbox = await RaceHelpers.is_won(LOOTBOX_CHANCE, self.race_obj.league, from_league=3)
        is_with_key = False
        if place < 4:
            is_with_key = await RaceHelpers.is_won(KEY_CHANCES[place], self.race_obj.league, from_league=3)
            await self._process_money(racer.user, place)
            await self._process_exp(racer.user, place)
            await self._process_medals(racer.user, place)
        elif place < 7:
            await self._process_money(racer.user, place)

        if self.race_obj.league >= POINTS_FROM_LEAGUE:
            await self._process_points(racer, place)
        await self._process_loot(racer.user, is_with_lootbox, is_with_key)
        await RaceMessager.send_graduation(racer.user, place, self.race_obj, won_racers, is_with_lootbox, is_with_key)

    async def _graduation(self):
        racers = await objects.execute(UserRacer.select().where(UserRacer.race == self.race_obj).order_by(UserRacer.main_time))
        tasks = []
        for place, racer in enumerate(racers, 1):
            tasks.append(self._graduate_user(racer, place, racers[:len(MONEY_FOR_RACE)]))
        await asyncio.gather(*tasks)

    async def start(self):
        await self._send_race_info()
        await asyncio.sleep(WAIT_BEFORE_RACE[self.race_obj.league])
        await self._process_tires()
        await self._send_lap_info()
        for lap in range(1, self.race_obj.laps+1):
            await asyncio.sleep(LAPS_INTERVALS[self.race_obj.league])
            await self._update_lap(lap)
            await self._send_lap_info(lap)
            logger.debug(f'Lap {lap}/{self.race_obj.laps} successfully')
        await self._graduation()
        await self.set_statuses_rest()
