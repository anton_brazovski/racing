AUTODROMES = [
    {
        'id': 2511174,
        'name': 'Santa Cruz de Tenerife',
        'country': 'ES',
        'length': 4634,
        'turns': 13,
        'straights': 4,
        'speed_index': 5,
        'tires_deterioration_value': 4,
        'flag': '🇪🇸',
        'coord': {
            'lon': -16.25462,
            'lat': 28.468241
        }
    },

    {
        "id": 1261835,
        "name": "Naravarikuppam",
        "country": "IN",
        'length': 3434,
        'turns': 11,
        'straights': 5,
        'speed_index': 4,
        'tires_deterioration_value': 4,
        'flag': '🇮🇳',
        "coord": {
          "lon": 80.174171,
          "lat": 13.19556
        }
    },

    {
        "id": 1627625,
        "name": "Sepang",
        "country": "MY",
        'flag': '🇲🇾',
        'length': 5543,
        'turns': 15,
        'straights': 6,
        'speed_index': 7,
        'tires_deterioration_value': 5,
        "coord": {
          "lon": 114.917099,
          "lat": -8.3089
        }
    },

    {
        "id": 2509954,
        "name": "Valencia",
        "country": "ES",
        'flag': '🇪🇸',
        'length': 2540,
        'turns': 11,
        'straights': 3,
        'speed_index': 6,
        'tires_deterioration_value': 2,
        "coord": {
          "lon": -0.37739,
          "lat": 39.469749
        }
    },

    {
        "id": 6356935,
        "name": "Jerez de la Frontera",
        "country": "ES",
        'flag': '🇪🇸',
        'length': 3300,
        'turns': 13,
        'straights': 6,
        'speed_index': 4,
        'tires_deterioration_value': 3,
        "coord": {
          "lon": -6.13277,
          "lat": 36.696869
        }
    },

    {
        "id": 1880251,
        "name": "Republic of Singapore",
        "country": "SG",
        'flag': '🇸🇬',
        'length': 5060,
        'turns': 23,
        'straights': 3,
        'speed_index': 2,
        'tires_deterioration_value': 2,
        "coord": {
            "lon": 103.800003,
            "lat": 1.36667
        }
    },

    {
        "id": 3175537,
        "name": "Imola",
        "country": "IT",
        'length': 4936,
        'turns': 17,
        'straights': 4,
        'speed_index': 7,
        'tires_deterioration_value': 4,
        'flag': '🇮🇹',
        "coord": {
            "lon": 11.71582,
            "lat": 44.352268
        }
    },

    {
        'id': 2993458,
        'name': 'Monaco',
        'country': 'MC',
        'length': 3337,
        'turns': 19,
        'straights': 3,
        'speed_index': 1,
        'tires_deterioration_value': 1,
        'flag': '🇮🇩',
        'coord': {
            'lon': 7.41667,
            'lat': 43.73333
        }
    },

    {
        'id': 6537122,
        'name': 'Monza',
        'country': 'IT',
        'length': 5793,
        'turns': 11,
        'straights': 4,
        'speed_index': 9,
        'tires_deterioration_value': 5,
        'flag': '🇮🇹',
        'coord': {
            'lon': 9.27485,
            'lat': 45.582481
        }
    },

    {
        'id': 745044,
        'name': 'Istanbul',
        'country': 'TR',
        'length': 5338,
        'turns': 14,
        'straights': 4,
        'speed_index': 6,
        'tires_deterioration_value': 3,
        'flag': '🇹🇷',
        'coord': {
            'lon': 28.949659,
            'lat': 41.01384
        }
    },

    {
        "id": 2637827,
        "name": "Silverstone",
        "country": "GB",
        'length': 5891,
        'turns': 18,
        'straights': 5,
        'speed_index': 4,
        'tires_deterioration_value': 3,
        'flag': '🇬🇧',
        "coord": {
            "lon": -1.02602,
            "lat": 52.092159
        }
    },

    {
        'id': 2786186,
        'name': 'Stavelot',
        'country': 'BE',
        'length': 7004,
        'turns': 20,
        'straights': 5,
        'speed_index': 5,
        'tires_deterioration_value': 3,
        'flag': '🇧🇪',
        'coord': {
            'lon': 5.93433,
            'lat': 50.392979
        }
    }
]
