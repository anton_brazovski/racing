from os import environ as env
import asyncio
from aiogram import Bot
from aiogram.dispatcher import Dispatcher
from aiogram.contrib.fsm_storage.redis import RedisStorage2
from decimal import Decimal
import system.credentials


storage = RedisStorage2(db=5)
loop = asyncio.get_event_loop()
bot = Bot(token=env['BOT_TOKEN'], parse_mode='html', loop=loop)
dp = Dispatcher(bot, storage=storage)


TEST = bool(env.get('TEST'))

START_FUEL = 100
FUEL_FOR_LVL = 10
REFUEL_AMOUNT = 5
START_MONEY = (1000, 1_000_000)[TEST]
REFERRAL_LEVELS_COUNT = 5

REFERRAL_INCOME = [
    Decimal('0.05'),
    Decimal('0.03'),
    Decimal('0.02'),
    Decimal('0.01'),
    Decimal('0.01')
]


DRIVER_SKILLS_BONUS = {
    'tire_control': 1,  # point to tires health
    'straights': 0.001,  # % to straights speed
    'turns': 0.001  # % to accelereation speed
}


MIN_RACERS_IN_GAME = (6, 3)[TEST]
MAX_RACERS_IN_GAME = (10, 10)[TEST]

ACTIVE_OR_NOT_SM = ['❌', '✅']

MONEY_FOR_RACE = {
    1: 1500,
    2: 800,
    3: 500,
    4: 300,
    5: 300,
    6: 300
}

WINNERS_MEDALS = ['🥇', '🥈', '🥉']
MEDALS = {1: 'gold', 2: 'silver', 3: 'bronze'}

if TEST:
    RACE_INTERVAL = {1: 1, 2: 1, 3: 1, 4: 1, 5: 1, 6: 1, 7: 1, 8: 1, 9: 1, 10: 1}
else:
    RACE_INTERVAL = {
        1: 2,
        2: 4,
        3: 6,
        4: 10,
        5: 10,
        6: 10,
        7: 10,
        8: 10,
        9: 10,
        10: 10
    }

LAPS = {
    1: 10,
    2: 15,
    3: 20,
    4: 25,
    5: 30,
    6: 35,
    7: 40,
    8: 45,
    9: 50,
    10: 55
}

LAPS_INTERVALS = {
    1: 15,
    2: 30,
    3: 30,
    4: 60,
    5: 60,
    6: 60,
    7: 60,
    8: 60,
    9: 60,
    10: 60
}
if TEST:
    LAPS_INTERVALS = {lvl: 5 for lvl in LAPS_INTERVALS.keys()}

WAIT_BEFORE_RACE = {
    1: 5,
    2: 15,
    3: 30,
    4: 60,
    5: 120,
    6: 240,
    7: 300,
    8: 300,
    9: 300,
    10: 300
}

POINTS = {
    1: 25,
    2: 18,
    3: 15,
    4: 12,
    5: 10,
    6: 8,
    7: 6,
    8: 4,
    9: 2,
    10: 1
}

POINTS_FROM_LEAGUE = 3

LANG_FLAGS = {'ru': '🇷🇺', 'en': '🇬🇧'}

PREM_DAYS = 30
PRICE_PREM = {
    'eth': 0.01,
    'btc': 0.0005,
}
VIP_EMOJI = '👑'
PREM_MONEY_BONUS = 2
PLAIN_ACCOUNT_PREFIX = '/u'
VIP_ACCOUNT_PREFIX = VIP_EMOJI + ' '

USERS_IN_TOP_RATING = 5
COUNTRY_FLAGS = {
    'ES': '🇪🇸',
    'GB': '🇬🇧',
    'IN': '🇮🇳',
    'MY': '🇲🇾',
    'SG': '🇸🇬',
    'IT': '🇮🇹',
    'MC': '🇮🇩',
    'TR': '🇹🇷',
    'BE': '🇧🇪'
}

BASE_SPEED = 70
BASE_SPEED_RAND_DIFF = 0.07


EXP_FOR_LAP = 5
RAND_DIFF_EXP = 1
EXP_FOR_PLACE = {1: 50, 2: 30, 3: 20}

ADMIN_ID = (1, 2)[TEST]

SPEED_BONUS_TRACK = Decimal('0.02')
SPEED_BONUS_LVL = Decimal('0.003')
SPEED_BONUS_LVL_LIMIT = Decimal('0.001')


CAR_SM_LVL = {
    '1️⃣': 1,
    '2️⃣': 2,
    '3️⃣': 3,
    '4️⃣': 4,
    '5️⃣': 5,
    '6️⃣': 6,
    '7️⃣': 7,
    '8️⃣': 8,
    '9️⃣': 9,
    '🔟': 10
}

CAR_LVL_SM = {
    0: '0️⃣',
    1: '1️⃣',
    2: '2️⃣',
    3: '3️⃣',
    4: '4️⃣',
    5: '5️⃣',
    6: '6️⃣',
    7: '7️⃣',
    8: '8️⃣',
    9: '9️⃣',
    10: '🔟'
}

LEAGUE_BUSTS = {
    1: 1,
    2: 2,
    3: 5,
    4: 9,
    5: 15,
    6: 20,
    7: 37,
    8: 50,
    9: 120,
    10: 260
}

LEAGUE_TIRES = {
    1: ['M'],
    2: ['M'],
    3: ['S', 'M', 'H'],
    4: ['HS', 'US', 'S', 'M', 'H', 'UH'],
    5: ['HS', 'US', 'S', 'M', 'H', 'UH'],
    6: ['HS', 'US', 'S', 'M', 'H', 'UH'],
    7: ['HS', 'US', 'S', 'M', 'H', 'UH'],
    8: ['HS', 'US', 'S', 'M', 'H', 'UH'],
    9: ['HS', 'US', 'S', 'M', 'H', 'UH'],
    10: ['HS', 'US', 'S', 'M', 'H', 'UH']
}

CANISTER = 20

HELP_USERNAME = 'RacingHelp'

TEAM_REQ_LVL = 25
TEAM_REQ_MONEY = 200_000
BASE_TEAM_LVL_LIMIT = 100

TEAM_COMISSION = .3  # 30%

TEAM_BUILDINGS_PRICE_MONEY = {
    1: 500_000,
    2: 1_000_000,
    3: 2_000_000,
    4: 5_000_000,
    5: 10_000_000,
    6: 20_000_000,
    7: 50_000_000,
    8: 100_000_000,
    9: 200_000_000,
    10: 500_000_000
}

TEAM_BUILDINGS_PRICE_HUMANHOURS = {
    1: 500,
    2: 1_000,
    3: 2_000,
    4: 5_000,
    5: 10_000,
    6: 20_000,
    7: 50_000,
    8: 100_000,
    9: 200_000,
    10: 500_000
}

STAFF_SALARY = 1000

MECHANIC_BONUS = 0.01
BASE_PITSTOP = 30

REFUELING_SPEED = {
    0: 0,
    1: 10,
    2: 15,
    3: 20,
    4: 30,
    5: 50,
    6: 70,
    7: 100,
    8: 150,
    9: 200,
    10: 250
}

METEO_POWER = {
    0: 0,
    1: 1,
    2: 2,
    3: 4,
    4: 6,
    5: 9,
    6: 13,
    7: 17,
    8: 25,
    9: 29,
    10: 35
}

MIN_CONTRACT_LEN = 5
MAX_CONTRACT_LEN = 30


STAFF_EMOJI = {'manager': '👨🏼‍💻', 'mechanic': '👨🏼‍🔧', 'builder': '👲🏼'}


LOOTBOX = '📦'
KEY = '🔪'


LOOTBOX_CHANCE = 8  # %
KEY_CHANCES = {
    1: 16,
    2: 14,
    3: 8
}  # %


TEAM_DAILY_WINS = {
    1: 500_000,
    2: 300_000,
    3: 150_000,
    4: 100_000,
    5: 50_000,
}


CHANCES = {
    'canister': 90,
    'VIP': 10
}
VIP_FOR_DAYS = 1



DEV_CHAT_ID = -1001354012700
INT_CHAT = 't.me/racing_int'
RU_CHAT = 't.me/racing_ru'
ID_CHAT = 't.me/racing_ind'

MINUTES_TO_ONLINE = 300
