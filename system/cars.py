CARS = {
    1: [
        {
            'name': 'Mazda3',
            'max_speed': 1,
            'acceleration': 1,
            'price': 5000,  # $
            'consumption': 1,  # l. per lap
        }
    ],

    2: [
        {
            'name': 'VW Golf',
            'max_speed': 2,
            'acceleration': 1,
            'price': 10000,
            'consumption': 2,
        },
        {
            'name': 'Renault Sandera',
            'max_speed': 1,
            'acceleration': 2,
            'price': 10000,
            'consumption': 2,
        },
        {
            'name': 'Kia Rio',
            'max_speed': 1,
            'acceleration': 1,
            'price': 10000,
            'consumption': 1,
        }
    ],

    3: [
        {
            'name': 'Suzuki SX4',
            'max_speed': 2,
            'acceleration': 2,
            'price': 50000,
            'consumption': 1,
        },

        {
            'name': 'Hyundai Creta',
            'max_speed': 2,
            'acceleration': 3,
            'price': 50000,
            'consumption': 3,
        },

        {
            'name': 'Ford Fiesta',
            'max_speed': 3,
            'acceleration': 2,
            'price': 50000,
            'consumption': 3,
        }
    ],

    4: [
        {
            'name': 'BMW E60',
            'max_speed': 3,
            'acceleration': 4,
            'price': 125000,
            'consumption': 3,
        },

        {
            'name': 'Renault Fluence',
            'max_speed': 3,
            'acceleration': 3,
            'price': 125000,
            'consumption': 2,
        },

        {
            'name': 'Hyundai Sonata',
            'max_speed': 4,
            'acceleration': 3,
            'price': 125000,
            'consumption': 4,
        }
    ],

    5: [
        {
            'name': 'Honda Civic R',
            'max_speed': 4,
            'acceleration': 6,
            'price': 190000,
            'consumption': 4,
        },

        {
            'name': 'VW Golf R',
            'max_speed': 5,
            'acceleration': 5,
            'price': 190000,
            'consumption': 4,
        },

        {
            'name': 'Mazda RX8',
            'max_speed': 6,
            'acceleration': 4,
            'price': 190000,
            'consumption': 7,
        },

        {
            'name': 'BMW E34',
            'max_speed': 5,
            'acceleration': 4,
            'price': 190000,
            'consumption': 3,
        }
    ],

    6: [
        {
            'name': 'Mitsubishi Lancer',
            'max_speed': 7,
            'acceleration': 6,
            'price': 300_000,
            'consumption': 5,
        },

        {
            'name': 'Lexus ES7',
            'max_speed': 5,
            'acceleration': 7,
            'price': 300_000,
            'consumption': 4,
        },

        {
            'name': 'Toyota Camry',
            'max_speed': 6,
            'acceleration': 6,
            'price': 300_000,
            'consumption': 5,
        },
    ],

    7: [
        {
            'name': 'Audi S4',
            'max_speed': 7,
            'acceleration': 7,
            'price': 500_000,
            'consumption': 5,
        },

        {
            'name': 'Volvo S60',
            'max_speed': 6,
            'acceleration': 8,
            'price': 500_000,
            'consumption': 6,
        },

        {
            'name': 'Subaru Legacy',
            'max_speed': 7,
            'acceleration': 8,
            'price': 500_000,
            'consumption': 7,
        },
    ],

    8: [
        {
            'name': 'Audi RS5',
            'max_speed': 7,
            'acceleration': 8,
            'price': 1_000_000,
            'consumption': 5,
        },

        {
            'name': 'Genesis G70',
            'max_speed': 8,
            'acceleration': 7,
            'price': 1_000_000,
            'consumption': 6,
        },

        {
            'name': 'Peugeot 508',
            'max_speed': 8,
            'acceleration': 8,
            'price': 1_000_000,
            'consumption': 5,
        },
    ],

    9: [
        {
            'name': 'Chevrolet Corvet C7',
            'max_speed': 9,
            'acceleration': 8,
            'price': 3_000_000,
            'consumption': 7,
        },

        {
            'name': 'Dodge Challenger SRT8',
            'max_speed': 8,
            'acceleration': 10,
            'price': 3_000_000,
            'consumption': 8,
        },

        {
            'name': 'Shelby Mustang GT500',
            'max_speed': 9,
            'acceleration': 9,
            'price': 3_000_000,
            'consumption': 9,
        },
    ],

    10: [
        {
            'name': 'Ford GT',
            'max_speed': 10,
            'acceleration': 8,
            'price': 5_000_000,
            'consumption': 9,
        },

        {
            'name': 'Corvette ZR1',
            'max_speed': 9,
            'acceleration': 10,
            'price': 5_000_000,
            'consumption': 10,
        },

        {
            'name': 'Bentley Continental GT',
            'max_speed': 9,
            'acceleration': 9,
            'price': 5_000_000,
            'consumption': 9,
        },

        {
            'name': 'McLaren P1',
            'max_speed': 9,
            'acceleration': 10,
            'price': 5_000_000,
            'consumption': 9,
        },

        {
            'name': 'Tesla Roadster II',
            'max_speed': 10,
            'acceleration': 10,
            'price': 10_000_000,
            'consumption': 6,
        },
    ],


}
