TIRES = {
    'HS': {
        'name': 'Hyper Soft',
        'durability': 30,
        'acceleration': 3.5,
        'speed': 0.5,
    },

    'US': {
        'name': 'Ultra Soft',
        'durability': 50,
        'acceleration': 3,
        'speed': 1,
    },

    'SS': {
        'name': 'Super Soft',
        'durability': 70,
        'acceleration': 2.5,
        'speed': 1.5,
    },

    'S': {
        'name': 'Soft',
        'durability': 100,
        'acceleration': 2,
        'speed': 1.8,
    },

    'M': {
        'name': 'Medium',
        'durability': 120,
        'acceleration': 1.5,
        'speed': 2.2,
    },

    'H': {
        'name': 'Hard',
        'durability': 150,
        'acceleration': 1,
        'speed': 2.5,
    },

    'UH': {
        'name': 'Ultra Hard',
        'durability': 200,
        'acceleration': 0.5,
        'speed': 2.8,
    }
}
