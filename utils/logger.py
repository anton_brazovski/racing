import logging


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger('racing')

# logging.getLogger('sqlalchemy.engine.base.Engine').addFilter(NoRunningFilter())

logging.getLogger('apscheduler.scheduler').setLevel(logging.INFO)
logging.getLogger('apscheduler.executors.default').setLevel(logging.ERROR)
