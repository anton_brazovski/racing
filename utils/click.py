from model import User, Clicks, objects, DoesNotExist
from utils.logger import logger


def click(method):
    async def clicked(*args, **kw):
        if not kw.get('user'):
            msg = kw.pop('msg')
            try:
                user = await objects.get(User, telegram_id=msg.from_user.id)
                await objects.create(Clicks, user=user, target=method.__name__)
                logger.debug(f'User {user.id} used {method.__name__}, args: {args}, kwargs: {kw}')
            except DoesNotExist:
                return await method(*args, **kw, msg=msg)
        else:
            user = kw.pop('user')
        return await method(*args, **kw, user=user)
    return clicked
