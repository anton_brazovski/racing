import asyncio
from aiogram import types
from aiogram.dispatcher import CancelHandler, DEFAULT_RATE_LIMIT, ctx
from aiogram.dispatcher.middlewares import BaseMiddleware
from aiogram.utils import context
from aiogram.utils.exceptions import Throttled
from uuid import uuid4
from decimal import Decimal
from system.settings import CAR_LVL_SM, FUEL_FOR_LVL, START_FUEL
import datetime
import time
import logging
import random


logger = logging.getLogger('TimeTracker')
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.FileHandler('timetracker.log'))
logger.propagate = False


def rate_limit(limit: int, key=None):
    """
    Decorator for configuring rate limit and key in different functions.
    :param limit:
    :param key:
    :return:
    """

    def decorator(func):
        setattr(func, 'throttling_rate_limit', limit)
        if key:
            setattr(func, 'throttling_key', key)
        return func

    return decorator


class ThrottlingMiddleware(BaseMiddleware):
    """
    Simple middleware
    """

    def __init__(self, limit=DEFAULT_RATE_LIMIT, key_prefix='antiflood_'):
        self.rate_limit = limit
        self.prefix = key_prefix
        super(ThrottlingMiddleware, self).__init__()

    async def on_process_message(self, message: types.Message):
        """
        This handler is called when dispatcher receives a message
        :param message:
        """
        # Get current handler
        handler = context.get_value('handler')

        # Get dispatcher from context
        dispatcher = ctx.get_dispatcher()

        # If handler was configured, get rate limit and key from handler
        if handler:
            limit = getattr(handler, 'throttling_rate_limit', self.rate_limit)
            key = getattr(handler, 'throttling_key', f"{self.prefix}_{handler.__name__}")
        else:
            limit = self.rate_limit
            key = f"{self.prefix}_message"

        try:
            await dispatcher.throttle(key, rate=limit)
        except Throttled as t:
            await self.message_throttled(message, t)
            raise CancelHandler()

    async def message_throttled(self, message: types.Message, throttled: Throttled):
        delta = throttled.rate - throttled.delta
        await asyncio.sleep(delta)


def get_ref_link():
    return str(uuid4().hex)[:10].replace('-', '')


def get_correct_value(num):
    if '.' in str(num):
        num = str(num).rstrip('0')
        if num[-1] == '.':
            num = num[:-1]
    return Decimal(num)


def get_user_place(user, users):
    for i, u in enumerate(users, 1):
        if u.id == user.id:
            return i, u
    else:
        return None, None


def get_index(ind):
    return CAR_LVL_SM[ind]


def get_max_fuel(user):
    return START_FUEL + user.lvl * FUEL_FOR_LVL


def get_next_hour():
    now = datetime.datetime.utcnow()
    return datetime.datetime(year=now.year, month=now.month, day=now.day, hour=now.hour+1, second=random.randint(0, 30))


def get_in_3_seconds():
    return datetime.datetime.utcnow() + datetime.timedelta(seconds=3)


def timeit(method):
    async def timed(*args, **kw):
        ts = time.time()
        result = await method(*args, **kw)
        te = time.time()
        logger.info(f'{method.__name__} {round((te - ts) * 1000, 2)} ms')
        return result
    return timed


async def get_win_by_chance(chances: dict):
    ranges_chances = {}
    previous_limit = 0
    for thing, chance in chances.items():
        for i in range(previous_limit, previous_limit+chance):
            ranges_chances[i] = thing
        previous_limit = previous_limit+chance
    return ranges_chances[random.randint(0, previous_limit-1)]


async def get_sum_of_skills_lvl(user):
    return user.tires_control + user.straights + user.turns

